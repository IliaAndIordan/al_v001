import { Component, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { MatSidenav, MatDialog } from '@angular/material';
import { CurrentUserService } from './@core/auth/current-user.service';
import { LoginModalDialog } from './@share/modal/login/login-modal.dialog';
import { AmsApplications } from './@core/const/ams-application.const';
import { Router } from '@angular/router';
import { AppRoutes } from './@core/const/app-routes.const';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  @ViewChild('sideLeft') sideLeft: MatSidenav;

  appl = AmsApplications.AMS;
  curtime: Date;
  curTimeInt: any;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  isAuthenticated: boolean;
  private userChangedSubscription: any;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private currUserService: CurrentUserService,
    public dialogService: MatDialog) {

    this.isAuthenticated = this.currUserService.isAuthenticated;
    this.userChangedSubscription = this.currUserService.UserChanged
      .subscribe(newUser => {
        this.isAuthenticated = this.currUserService.isAuthenticated;
      });

    //  get current date time, you can set your own Date() over here
    // let stationdate = new Date(date_from_dropdown_change);
    this.curtime = new Date();
    // clear Interval, it's my system requirement
    if (this.curTimeInt) {
        clearInterval(this.curTimeInt);
    }

    // set clock and Interval
    this.curTimeInt  = setInterval(() => {
        // stationdate = new Date(stationdate.setSeconds(stationdate.getSeconds() + 1));
        this.curtime = new Date();
    }, 1000);
  }

  sideLeftToggle() {
    this.sideLeft.toggle();
  }

  //#region Login/User Profile

  public onProfileClick(): void {
    if (this.currUserService.isAuthenticated) {
      this.router.navigate(['/', AppRoutes.Airline]);
    } else {
      this.loginDialogShow();
    }
  }


  public loginDialogShow(): void {
    const dialogRef = this.dialogService.open(LoginModalDialog, {
      width: '500px',
      height: '320px',
      data: { email: null, password: null }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      // this.password = result.password;
    });
  }

  //#endregion
}
