import { Component, OnInit, OnDestroy } from '@angular/core';
import { AirlineModel, UserModel } from 'src/app/@core/auth/api/dto';
import { AirportModel } from 'src/app/@core/api/airport/dto';
import { AcMacModel, AcMfrModel } from 'src/app/@core/api/aircraft/dto';
import { BreakpointObserver } from '@angular/cdk/layout';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { AirlineService } from 'src/app/@core/api/airline/airline.service';
import { AcMfrService } from 'src/app/@core/api/aircraft/ac-mfr.service';
import { AcMacService } from 'src/app/@core/api/aircraft/ac-mac.service';
import { MatDialog } from '@angular/material';

@Component({
    templateUrl: './manufacturer.component.html',
    styleUrls: ['./manufacturer.component.scss']
})

export class ManufacturerComponent implements OnInit, OnDestroy {

    airline: AirlineModel;
    hqAirport: AirportModel;
    private airlineChanged: any;

    user: UserModel;
    manufacturer: AcMfrModel;
    private manufacturerChanged: any;
    subscriptionAcMacList;
    acMacList: Array<AcMacModel>;

    constructor(
        private breakpointObserver: BreakpointObserver,
        private currUserService: CurrentUserService,
        private alService: AirlineService,
        private acMfrService: AcMfrService,
        private acMacService: AcMacService,
        public dialogService: MatDialog) {

    }

    ngOnInit(): void {
        this.user = this.currUserService.user;

        this.airline = this.currUserService.airline;
        this.airlineChanged = this.currUserService.AirlineChanged.subscribe(newAirline => {
            console.log('airlineChanged->', newAirline);
            this.airline = newAirline;
            this.updateFields();
        });

        this.manufacturer = this.acMfrService.currMfr;
        this.manufacturerChanged = this.acMfrService.currMfrChanged.subscribe(newMfr => {
            console.log('newMfr->', newMfr);
            this.manufacturer = newMfr;
            this.loadAcMacList();
        });
        this.loadAcMacList();
    }

    ngOnDestroy(): void {

        if (this.airlineChanged) { this.airlineChanged.unsubscribe(); }
        if (this.manufacturerChanged) { this.manufacturerChanged.unsubscribe(); }
    }

    updateFields() {

    }

    loadAcMacList(): void {

        if (this.manufacturer) {
            this.acMacService.acMacByMfr(this.manufacturer.mfr_id)
                .subscribe((resp: Array<AcMacModel>) => {
                    this.acMacList = resp;
                    console.log('acMacList->', resp);
                    this.updateFields();
                });
        } else { this.acMacList = new Array<AcMacModel>(); }


    }

    refreshAcMacList() {
        if (this.manufacturer) {
            this.acMacService.resetAcMacCache();
            this.loadAcMacList();
        }
    }
}
