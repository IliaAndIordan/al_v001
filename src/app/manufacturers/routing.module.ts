import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// ---
// Local Components
import { AppRoutes } from '../@core/const/app-routes.const';

import { HomeComponent } from './home.component';
import { AuthGuard } from '../@core/guards/aut-guard.service';
import { ManufacturersComponent } from './manufacturers.component';
import { ManufacturerComponent } from './manufacturer/manufacturer.component';
import { MacComponent } from './mac/mac.component';


const routes: Routes = [
    {
        path: '', // AircraftComponent lazy loaded module, must be empty string, Root route expressed in app-routing.module.ts
        component: ManufacturersComponent,
        canActivate: [AuthGuard],
        children: [
          { path: '', component: HomeComponent },
          { path: 'mfr', component: ManufacturerComponent ,  canActivate: [AuthGuard]},
          { path: 'mac/:macId', component: MacComponent ,  canActivate: [AuthGuard]}
        ]
      }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ManufacturersRoutingModule { }
export const RoutedComponents = [ManufacturersComponent, HomeComponent, MacComponent];
