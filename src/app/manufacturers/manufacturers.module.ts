import { NgModule } from '@angular/core';
import { GoogleChartsModule } from 'angular-google-charts';
// Local Modules
import { SharedModule } from '../@share/shared.module';
import { RoutedComponents, ManufacturersRoutingModule } from './routing.module';
import { CommonModule } from '@angular/common';
import { GMAP_API_KEY } from '../@core/const/ams-storage-key.const';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Components
import { PageHeaderComponent } from './page-header/page-header.component';
import { ManufacturerComponent } from './manufacturer/manufacturer.component';
import { MacComponent } from './mac/mac.component';
import { MacPageHeaderComponent } from './mac/page-header/page-header.component';

@NgModule({
    imports: [
        CommonModule,
        ManufacturersRoutingModule,
        SharedModule,
        GoogleChartsModule.forRoot(GMAP_API_KEY),
        //
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        RoutedComponents,
        //
        PageHeaderComponent,
        ManufacturerComponent,
        MacComponent,
        MacPageHeaderComponent,
    ],
    exports: [
        MacPageHeaderComponent,
    ],
    entryComponents: [

    ]
})
export class ManufacturersModule { }

