import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

// Models and services
import { AcMacModel, AcMfrModel } from 'src/app/@core/api/aircraft/dto';
import { AcMacService } from 'src/app/@core/api/aircraft/ac-mac.service';
import { AcMfrService } from 'src/app/@core/api/aircraft/ac-mfr.service';
import { subscribeOn } from 'rxjs/operators';

@Component({
    templateUrl: './mac.component.html',
})

export class MacComponent implements OnInit, OnDestroy {

    mac: AcMacModel;
    mfr: AcMfrModel;
    macId: number;

    constructor(private router: Router,
        private activatedRoute: ActivatedRoute,
        public toastr: ToastrService,
        private acMacService: AcMacService,
        private acMfrService: AcMfrService) { }

    ngOnInit(): void {
        this.activatedRoute.params
            .subscribe((param: Params) => {
                // Also get the manufacturerId.
                this.macId = parseInt(param.macId, 10);
                console.log('macId', this.macId);
                this.initFields();
            });

    }

    ngOnDestroy(): void {

    }

    initFields() {
        
        if (this.macId) {
            console.log('initFields macId', this.macId);
            console.log('macId', this.macId);
            this.acMacService.acMac(this.macId)
                .subscribe((res: Array<AcMacModel>) => {
                    if (res && res.length > 0) {
                        console.log('acMac', res);
                        this.mac = res[0];
                    }

                    if (this.mac) {
                        this.acMfrService.acMfr(this.mac.mfr_id)
                            .subscribe((resMfr: Array<AcMfrModel>) => {
                                console.log('resMfr', resMfr);
                                if (resMfr && resMfr.length > 0) {
                                    this.mfr = resMfr[0];
                                }
                            });
                    }
                });
        }
    }
}
