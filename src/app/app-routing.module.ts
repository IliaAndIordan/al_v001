import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Constants
// ---Components
import { PreloadSelectedModuleList } from './@core/preload-strategy';
import { AppRoutes } from './@core/const/app-routes.const';
import { AuthGuard } from './@core/guards/aut-guard.service';

export const routes: Routes = [
  { path: AppRoutes.Root, pathMatch: 'full', redirectTo: AppRoutes.Public },
  { path: AppRoutes.Public, loadChildren: './public/public.module#PublicModule', data: { preload: true } },
  { path: AppRoutes.Newspaper, loadChildren: './newspaper/newspaper.module#NewspaperModule', data: { preload: true } },
  // Loggin
  { path: AppRoutes.Dashboard, loadChildren: './public/public.module#PublicModule', data: { preload: true }, canActivate: [AuthGuard] },
  // Modules
  { path: AppRoutes.Airline, loadChildren: './airline/airline.module#AirlineModule', data: { preload: true }, canActivate: [AuthGuard] },
  {path: AppRoutes.Aircrafts, loadChildren: './aircraft/aircraft.module#AircraftModule',
    data: { preload: true }, canActivate: [AuthGuard]
  },
  {path: AppRoutes.Manufacturers, loadChildren: './manufacturers/manufacturers.module#ManufacturersModule', data: { preload: true } },

  // { path: BwtRoutes.Public, loadChildren: './public/public.module#PublicModule', data: { preload: true } },


];

@NgModule({

  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadSelectedModuleList, enableTracing: false })
  ],
  exports: [RouterModule],
  providers: [PreloadSelectedModuleList]

})
export class AppRoutingModule { }

export const routableComponents = [];

