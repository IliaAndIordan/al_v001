import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
// Constants
import { IMG_WIKI_URL, IMG_OUAP_URL } from 'src/app/@core/const/ams-storage-key.const';
// Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { AirlineModel, UserModel } from 'src/app/@core/auth/api/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AuthService } from 'src/app/@core/auth/api/auth.service';


@Component({
    selector: 'app-aircraft-header',
    templateUrl: './page-header.component.html',
})

export class PageHeaderComponent implements OnInit, OnDestroy {


    wikiUrl = IMG_WIKI_URL;
    ourApUrl = IMG_OUAP_URL;

    user: UserModel;
    userChanged: any;
    isAdmin: boolean;

    constructor(
        private router: Router,
        private cus: CurrentUserService,
        private authService: AuthService) { }

    ngOnInit(): void {
        this.user = this.cus.user;
        this.isAdmin = this.cus.isAdmin;
        this.userChanged = this.cus.UserChanged.subscribe(newUser => {
            console.log('newUser->', newUser);
            this.user = newUser;
            this.isAdmin = this.cus.isAdmin;
        });
    }

    ngOnDestroy(): void {
        if (this.userChanged) {
            this.userChanged.unsubscribe();
        }
    }

    //#region Navigation

    gotoAircraftAdmin() {
        if (this.cus.user && this.cus.isAdmin) {
            this.router.navigate(['/', AppRoutes.Aircrafts]);
        } else {
            this.logout();
        }

    }

    logout(): void {
        this.authService.logout();
    }


    //#endregion


}
