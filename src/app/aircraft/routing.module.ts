import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// ---
// Local Components
import { AppRoutes } from '../@core/const/app-routes.const';

import { HomeComponent } from './home.component';
import { AircraftComponent } from './aircraft.component';
import { AuthGuard } from '../@core/guards/aut-guard.service';


const routes: Routes = [
    {
        path: '', // AircraftComponent lazy loaded module, must be empty string, Root route expressed in app-routing.module.ts
        component: AircraftComponent,
        canActivate: [AuthGuard],
        children: [
          { path: '', component: HomeComponent },
          // { path: 'add', component: EditUserComponent },
          // { path: 'edit/:userId', component: EditUserComponent }
        ]
      }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AircraftRoutingModule { }
export const RoutedComponents = [AircraftComponent, HomeComponent];
