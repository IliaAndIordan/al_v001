import { Component, OnInit } from '@angular/core';

@Component({
    template: '<router-outlet></router-outlet>',
})

export class AircraftComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
