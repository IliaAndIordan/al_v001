import { NgModule } from '@angular/core';
import { GoogleChartsModule } from 'angular-google-charts';
// Local Modules
import { SharedModule } from '../@share/shared.module';
import { RoutedComponents, AircraftRoutingModule } from './routing.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { GMAP_API_KEY } from '../@core/const/ams-storage-key.const';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Components
import { PageHeaderComponent } from './page-header/page-header.component';

@NgModule({
    imports: [
        CommonModule,
        AircraftRoutingModule,
        SharedModule,
        GoogleChartsModule.forRoot(GMAP_API_KEY),
        //
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        RoutedComponents,
        //
        PageHeaderComponent

    ],
    exports: [
    ],
    entryComponents: [

    ]
})
export class AircraftModule { }

