import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointState, BreakpointObserver } from '@angular/cdk/layout';
import { Chart } from 'chart.js';
import { AirportService } from '../@core/api/airport/airport.service';
import { GoogleChartComponent } from 'angular-google-charts';
import { CurrentUserService } from '../@core/auth/current-user.service';
import { AirlineModel, UserModel } from '../@core/auth/api/dto';
import { ResponseCountryListModel } from '../@core/api/airport/dto';
import { SUCCESS } from '../@core/const/global.const';
import { MatDialog } from '@angular/material';
import { CreateAirlineModalDialog } from './modal/create-airline/create-airline-modal.dialog';
import { AirlineService } from '../@core/api/airline/airline.service';
import { ResponseAirlineModel } from '../@core/api/airline/dto';
import { Subscribable, Observable, Subscription } from 'rxjs';
import { GMAP_API_KEY } from '../@core/const/ams-storage-key.const';
import { EditAlSloganModalDialog } from './modal/edit-al-slogan/edit-al-slogan-modal.dialog';
import { IMG_ICAO_URL, IMG_IATA_URL, IMG_WIKI_URL, IMG_OUAP_URL } from 'src/app/@core/const/ams-storage-key.const';
import { CountryModel, CountryStateModel, AirportModel, IAirportModel, ResponseAirportListModel } from '../@core/api/airport/dto';
import { AirportDataService } from '../@core/api/airport/airport-data.service';


@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {

  colspan4Card = 3;
  matGridCollspan_4 = 4;
  icaoUrl = IMG_ICAO_URL;

  pvHeaderImageUrl = 'https://common.iordanov.info/images/airline/logo_0.png';
  airline: AirlineModel;
  hqAirport: AirportModel;
  hqAirportImgUrl: string;
  private airlineChanged: any;

  user: UserModel;
  subscriptionAirportList;
  airportList: Array<AirportModel>;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private currUserService: CurrentUserService,
    private airportService: AirportService,
    private apDs: AirportDataService,
    private alService: AirlineService,
    public dialogService: MatDialog) {

  }

  ngOnInit(): void {
    this.airline = this.currUserService.airline;
    this.user = this.currUserService.user;
    this.loadAirport();
    this.breakpointObserverInit();
    this.airlineChanged = this.currUserService.AirlineChanged.subscribe(newAirline => {
      console.log('airlineChanged->', newAirline);
      this.airline = newAirline;
      this.loadAirport();
    });

  }

  ngOnDestroy(): void {

    if (this.airlineChanged) {
      this.airlineChanged.unsubscribe();
    }
  }

  breakpointObserverInit(): void {
    this.breakpointObserver
      .observe([Breakpoints.Handset])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          console.log('Matches Handset viewport  mode');
          this.matGridCollspan_4 = 12;
          this.colspan4Card = 12;
        }
      });
    this.breakpointObserver
      .observe([Breakpoints.Tablet])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          console.log('Matches Tablet viewport mode');
          this.matGridCollspan_4 = 6;
          this.colspan4Card = 6;
        }
      });

    this.breakpointObserver
      .observe([Breakpoints.Web])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          console.log('Matches Web viewport mode');
          this.matGridCollspan_4 = 4;
          this.colspan4Card = 3;
        }
      });
    this.breakpointObserver
      .observe([Breakpoints.Small])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          console.log('Matches Handset viewport  mode');
          this.matGridCollspan_4 = 12;
          this.colspan4Card = 12;
        }
      });

  }

  updateFields(): void {
    if (this.airline) {

      if (this.airline.airline_id > 0) {
        this.pvHeaderImageUrl = this.airline.logoUrl;
      }
    }
    if (this.hqAirport) {
      this.hqAirportImgUrl = 'https://maps.googleapis.com/maps/api/staticmap?key=';
      this.hqAirportImgUrl += GMAP_API_KEY;
      this.hqAirportImgUrl += '&size=351x190';
      this.hqAirportImgUrl += '&maptype=terrain';
      this.hqAirportImgUrl += '&markers=size:mid%7Ccolor:green%7Clabel:H%7C';
      this.hqAirportImgUrl += this.hqAirport.lat + ',' + this.hqAirport.lon;
      this.hqAirportImgUrl += '&zoom=5';
      // https://maps.googleapis.com/maps/api/staticmap?
      // key=AIzaSyDFFY0r9_fLApc2awmt-O1Q2URpkSFbQVc&size=351x190&maptype=terrain
      // &markers=size:mid%7Ccolor:green%7Clabel:D%7C42.695,23.4083&zoom=10
    }
    console.log('updateFields hq airport:', this.hqAirport);
  }

  ngAfterViewInit(): void {

  }

  public createAirlineDialogShow(): void {
    const dialogRef = this.dialogService.open(CreateAirlineModalDialog, {
      data: { al_name: null, al_code: null }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      // this.password = result.password;
    });
  }

  public editAlSloganDialogShow(): void {
    const dialogRef = this.dialogService.open(EditAlSloganModalDialog, {
      data: this.airline
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      // this.password = result.password;
    });
  }


  //#region Data

  loadAirport(): void {

    if (this.airline && this.airline.hq_airport_id > 0) {
      this.apDs.airportById(this.airline.hq_airport_id)
        .subscribe((airport: AirportModel) => {
          console.log('loadAirport->', airport);
          this.hqAirport = airport;
          this.updateFields();
        });

    }
  }

  /*
    loadAirportList() {
      this.airportList = new Array<AirportModel>();
      this.subscriptionAirportList = this.airportService
        .getAirportListDbAll()
        .subscribe(
          (res: ResponseAirportListModel) => {
            if (res.status === SUCCESS) {
              res.data.airport_list.forEach((element: IAirportModel) => {
                const tmp: AirportModel = AirportModel.fromJSON(element);
                this.airportList.push(tmp);
              });
              this.updateFields();
            }

          },
          error => console.log('loadAirportList: Error: ', error)
        );
    }
  */
  getAirline(): void {
    // this.countyList = new Array<CountryMiniChartViewModel>();
    console.log('getAirline: airline_id=', this.currUserService.airline);
    if (this.airline && this.airline.airline_id) {


      this.alService.getAirline(this.airline.airline_id)
        .subscribe((responce: ResponseAirlineModel) => {
          console.log('getAirportListDbAll-> responce ', responce);
          if (responce && responce.status === SUCCESS) {
            const al: AirlineModel = AirlineModel.fromJSON(responce.data.airline);
            if (al && al.airline_id > 0) {
              this.currUserService.airline = al;
            }

          }
        });

      /*
    if (responce.data.countyList) {
      responce.data.countyList.forEach((element: ICountryModel) => {
        const tmp: CountryModel = CountryModel.fromJSON(element);
        const mcData = CountryMiniChartViewModel.fromCountryModel(tmp);
        this.countyList.push(mcData);
      });
    }
    */
      // console.log('getCountryList-> countyList', this.countyList);
    }
  }

  //#endregion

}
