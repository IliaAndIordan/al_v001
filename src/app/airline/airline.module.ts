import { NgModule } from '@angular/core';
import { GoogleChartsModule } from 'angular-google-charts';
// Local Modules
import { SharedModule } from '../@share/shared.module';
import { RoutedComponents, AirlineRoutingModule } from './routing.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { GMAP_API_KEY } from '../@core/const/ams-storage-key.const';
import { StepOneComponent } from './modal/create-airline/step-one/step-one.component';
import { MatStepperModule, MatFormFieldModule } from '@angular/material';
import { StepTwoComponent } from './modal/create-airline/step-two/step-two.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateAirlineModalDialog } from './modal/create-airline/create-airline-modal.dialog';
import { EditAlSloganModalDialog } from './modal/edit-al-slogan/edit-al-slogan-modal.dialog';
import { EditAirlineDialogComponent } from './modal/edit-airline/edit-airline.dialog';

@NgModule({
    imports: [
        CommonModule,
        AirlineRoutingModule,
        SharedModule,
        GoogleChartsModule.forRoot(GMAP_API_KEY),
        //
        MatStepperModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        RoutedComponents,
        CreateAirlineModalDialog,
        EditAlSloganModalDialog,
        EditAirlineDialogComponent,
        StepOneComponent,
        StepTwoComponent,
    ],
    exports: [
    ],
    entryComponents: [
        CreateAirlineModalDialog,
        EditAlSloganModalDialog,
        EditAirlineDialogComponent,
    ]
})
export class AirlineModule { }

