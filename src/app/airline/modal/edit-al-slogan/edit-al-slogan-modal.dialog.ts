import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, ErrorStateMatcher } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormGroupDirective, NgForm, FormControl, Validators } from '@angular/forms';
// Services
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AirlineService } from 'src/app/@core/api/airline/airline.service';
import { AirlineModel } from 'src/app/@core/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
// Constants

export interface LoginData {
  email: string;
  password: string;
}

export class SloganStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}


@Component({
  selector: 'app-edit-al-slogan-modal',
  templateUrl: './edit-al-slogan-modal.dialog.html',
  styleUrls: ['./edit-al-slogan-modal.dialog.scss']
})

// tslint:disable-next-line:component-class-suffix
export class EditAlSloganModalDialog implements OnInit {

  alData: AirlineModel;
  form: FormGroup;
  hasSpinner = false;
  slogan: string;
  sloganFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(6),
  ]);

  sloganMatcher = new SloganStateMatcher();
  errorMessage: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private alService: AirlineService,
    private currUserService: CurrentUserService,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<AirlineModel>,
    @Inject(MAT_DIALOG_DATA) public data: AirlineModel) {
    this.alData = data;
    this.slogan = data.slogan;
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      slogan: this.sloganFormControl,
    });
  }

  onNoClick(): void {
    // this.dialogRef.close();
  }

  onLoginClick() {

    if (this.form.valid) {
      this.errorMessage = undefined;
      this.hasSpinner = true;
      this.alData.slogan = this.form.value['slogan'];
      this.alService.alSave(this.alData)
        .subscribe(resAl => {
          if (resAl && resAl.status === 'success') {

            this.toastr.success('Airline Updated', 'Slogan updated to: ' +
              resAl.data.airline.slogan + '!');
            this.hasSpinner = false;
            setTimeout((router: Router) => {
              console.log('airline', resAl.data.airline);
              if (!resAl.data.airline) {
                this.currUserService.airline = AirlineModel.fromJSON(resAl.data.airline);
                this.router.navigate(['/', AppRoutes.Airline]);
              } else {
                this.router.navigate(['/', AppRoutes.Dashboard]);
              }
            }, 300);
            this.dialogRef.close(resAl.data.airline);
          } else {
            this.errorMessage = 'Airline Update Failed. ' + resAl.message;
            this.hasSpinner = false;
            setTimeout((router: Router) => {
              this.errorMessage = undefined;
            }, 2000);
            // this.spinerService.display(false);
            this.toastr.error('Airline Update Failed', resAl.message);
          }

        });
    } else {
      this.errorMessage = 'Not valid input';
      this.toastr.error('Please enter valid values for fields', 'Not valid input');
    }
  }


}
