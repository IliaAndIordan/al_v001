import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl, FormControl } from '@angular/forms';
import { CreateAirlineFormService } from '../create-airline.formservice';


@Component({
    selector: 'app-step-one',
    templateUrl: './step-one.component.html',
    // styleUrls: ['./step-one.component.css']
})

export class StepOneComponent {

    step: FormGroup;

    alNameFormControl = new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        StepOneComponent.checkCategoryInput(this.formService.regAlNames)
    ]);
    alCodeFormControl = new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(256),
        StepOneComponent.checkCategoryInput(this.formService.regCode)
    ]);

    public static checkCategoryInput(categoryList: any[]): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            const val = control.value;
            console.log('checkCategoryInput-> control: ', control);
            let rv = null;
            if (categoryList && categoryList.length > 0) {
                rv = name && categoryList.filter(x => x === val) &&
                    categoryList.filter(x => x === val).length > 0 ? null : { 'identityRevealed': true };
            }
            console.log('checkCategoryInput-> rv: ', rv);
            return rv;
        };
    }



    constructor(
        private _formBuilder: FormBuilder,
        private formService: CreateAirlineFormService) {
        this.step = this._formBuilder.group({
            al_name: this.alNameFormControl,
            al_code: this.alCodeFormControl,
        });
        this.formService.stepReady(this.step, 'one');
    }

    change(title) {
        this.step.patchValue({ extraName: title });
    }

    getAlCodeErrorMessage() {
        return this.alCodeFormControl.hasError('required') ? 'Valid 3 letter airline code is required.' :
            this.alCodeFormControl.hasError('minlength') ? 'Airline code is 3 letter - found ' + this.alCodeFormControl.value.length :
                this.alCodeFormControl.hasError('maxlength') ? 'Airline code is 3 letter - found ' + this.alCodeFormControl.value.length :
                    this.alCodeFormControl.hasError('checkcategoryinput') ?
                        'Airline code ' + this.alCodeFormControl.value + ' already registered' :
                        '';
    }

    getAlNameErrorMessage() {
        return this.alNameFormControl.hasError('required') ? 'Airline name is required.' :
            this.alNameFormControl.hasError('minlength') ?
                'Valid Airline Name is at least 6 letter - found ' + this.alNameFormControl.value.length :
                this.alNameFormControl.hasError('maxlength') ?
                    'Valid Airline code is max 256 letter - found ' + this.alNameFormControl.value.length :
                    this.alNameFormControl.hasError('checkcategoryinput') ?
                        'Airline name ' + this.alNameFormControl.value + ' already registered' :
                        '';
    }

}
