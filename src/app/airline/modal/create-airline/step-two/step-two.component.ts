import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CreateAirlineFormService } from '../create-airline.formservice';
import { AirportService } from 'src/app/@core/api/airport/airport.service';
import { ResponseAirportListModel, AirportViewModel, IAirportViewModel } from 'src/app/@core/models/airpport.view-model';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
    selector: 'app-step-two',
    templateUrl: './step-two.component.html',
    // styleUrls: ['./step-two.component.css']
})

export class StepTwoComponent implements OnInit {

    step: FormGroup;
    airportFormControl = new FormControl('', [
        Validators.required,
    ]);
    airportListDbAll: Array<AirportViewModel>;
    filteredAp: Observable<AirportViewModel[]>;
    constructor(
        private _formBuilder: FormBuilder,
        private formService: CreateAirlineFormService,
        private apService: AirportService) {
        this.step = this._formBuilder.group({
            hq_airport: this.airportFormControl
        });
        this.formService.stepReady(this.step, 'two');
        this.filteredAp = this.step.controls['hq_airport'].valueChanges.pipe(
                // startWith<string | AirportViewModel>(''),
                map(value => this._filter(value))
                // map(value => typeof value === 'string' ? value : value.sname),
                // map(sname => sname ? this._filter(sname) : this.airportListDbAll.slice())
        );
        /*
        this.filteredAp = this.formService.mainForm.controls['hq_airport_id'].valueChanges
            .pipe(
                // startWith<string | AirportViewModel>(''),
                map(value => typeof value === 'string' ? value : value.sname),
                map(sname => sname ? this._filter(sname) : this.airportListDbAll.slice())
            );
            */
    }

    ngOnInit() {
        this.getAirportListDbAll();
    }

    private _filter(value: string): AirportViewModel[] {
        console.log('_filter-> value: ', value);
        let data = new Array<AirportViewModel>();

        if (value && value.length > 2) {
            const filterValue = value.toLowerCase();
            data = this.airportListDbAll.filter(x => x.sname.toLowerCase().includes(filterValue));
        }
        return data;
    }

    displayAp(ap?: AirportViewModel): string | undefined {
        return ap ? ap.sname : undefined;
    }
    //#region Data

    getAirportListDbAll(): void {
        this.airportListDbAll = new Array<AirportViewModel>();

        this.apService.getAirportListDbAll()
            .subscribe((responce: ResponseAirportListModel) => {
                console.log('getAirportListDbAll-> responce ', responce);
                if (responce.data.airports_db_all) {
                    responce.data.airports_db_all.forEach((element: IAirportViewModel) => {
                        const tmp: AirportViewModel = AirportViewModel.fromJSON(element);
                        this.airportListDbAll.push(tmp);
                    });
                }

            });
    }
    //#endregion
}
