import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, ErrorStateMatcher } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import {
  FormGroup, FormBuilder, FormGroupDirective,
  NgForm, FormControl, Validators
} from '@angular/forms';
// Services
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AuthService } from 'src/app/@core/auth/api/auth.service';
import { CreateAirlineFormService } from './create-airline.formservice';
import { AlNameCodeModel, AirlineCreateData } from 'src/app/@core/api/airline/dto';
import { StepOneComponent } from './step-one/step-one.component';
import { StepTwoComponent } from './step-two/step-two.component';
import { AirportViewModel } from 'src/app/@core/models/airpport.view-model';
import { AirlineService } from 'src/app/@core/api/airline/airline.service';
import { SUCCESS } from 'src/app/@core/const/global.const';
// Constants

export interface AirlineData {
  airline_id: number;
  user_id: number;
  al_name: string;
  al_code: string;
  al_logo: string;
  al_slogan: string;
  al_description: string;
  hq_airport_id: number;
  adate: Date;
  udate: Date;
}


export class NameStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

export class IcaoCodeStateMacher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}


@Component({
  selector: 'app-create-airline-modal.dialog',
  templateUrl: './create-airline-modal.dialog.html',
  styleUrls: ['./create-airline-modal.dialog.scss'],
  providers: [CreateAirlineFormService]
})

// tslint:disable-next-line:component-class-suffix
export class CreateAirlineModalDialog implements OnInit {

  @ViewChild(StepOneComponent) firstFormGroup: StepOneComponent;
  @ViewChild(StepTwoComponent) seccondFormGroup: StepTwoComponent;

  hasSpinner = false;
  airline: AirlineCreateData;

  secondFormGroup: FormGroup;
  therdFormGroup: FormGroup;

  nameFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(6),
  ]);
  codeFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(2),
    Validators.maxLength(2),
  ]);

  nameMatcher = new NameStateMatcher();
  codeMatcher = new IcaoCodeStateMacher();
  errorMessage: string;

  constructor(
    private fb: FormBuilder,
    public formService: CreateAirlineFormService,
    private router: Router,
    private alService: AirlineService,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<CreateAirlineModalDialog>,
    @Inject(MAT_DIALOG_DATA) public data: AirlineData) {

    this.airline = this.formService.mainForm.value;
  }

  get frmStepOne() {
    return this.firstFormGroup ? this.firstFormGroup.step : null;
  }

  get frmStepTwo() {
    return this.seccondFormGroup ? this.seccondFormGroup.step : null;
  }

  ngOnInit(): void {
  }

  keys(): Array<string> {
    return Object.keys(this.airline);
  }

  onNoClick(): void {
    // this.dialogRef.close();
  }

  onSubmitClick() {

    if (this.formService.mainForm.valid) {
      this.errorMessage = undefined;
      this.hasSpinner = true;
      this.alService.alCreate(this.formService.mainForm.value)
        .subscribe(
          res => {
          console.log('alCreate', res);
          this.hasSpinner = false;
          if (res && res.status === SUCCESS) {
          } else {
            this.errorMessage = 'Create Airline Failed:' + res.message;
          }

        },
        error => {
          console.log('alCreate error: ', error);
          this.errorMessage = 'Create Airline Failed: ' +
           error.error.status + ' ' + error.error.statusCode + ':' + error.error.message;
      });
    } else {
      this.errorMessage = 'Not valid input';
      this.toastr.error('Please enter valid values for fields', 'Not valid input');
    }

  }


}
