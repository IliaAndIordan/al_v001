import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { AirlineService } from 'src/app/@core/api/airline/airline.service';
import { AlNameCodeListAll, AlNameCodeModel, ResponseAlNameCodeListModel } from 'src/app/@core/api/airline/dto';
import { ErrorStateMatcher } from '@angular/material';


@Injectable()
export class CreateAirlineFormService {

    private stepOneSource: Subject<FormGroup> = new Subject();
    stepOne: Observable<FormGroup> = this.stepOneSource.asObservable();

    private stepTwoSource: Subject<FormGroup> = new Subject();
    stepTwo: Observable<FormGroup> = this.stepTwoSource.asObservable();

    mainForm: FormGroup = this._formBuilder.group({
        al_name: '',
        al_code: '',
        hq_airport: '',
    });

    airlineNamesForCheck: Array<AlNameCodeModel>;
    public regAlNames: string[];
    public regCode: string[];

    constructor(
        private _formBuilder: FormBuilder,
        private alService: AirlineService) {
        this.airlineNamesForCheck = new Array<AlNameCodeModel>();
        this.regAlNames = new Array<string>();
        this.regCode = new Array<string>();
        this.alService.getAlListForCheckl().subscribe((res: ResponseAlNameCodeListModel) => {
            this.airlineNamesForCheck = res.data.al_list_for_check;
            if (this.airlineNamesForCheck && this.airlineNamesForCheck.length > 0) {
                this.airlineNamesForCheck.forEach(element => {
                    this.regAlNames.push(element.al_name);
                    this.regCode.push(element.al_code);
                });
            }
        });

        this.stepOne.subscribe(form =>
            form.valueChanges.subscribe(val => {
                console.log(val);
                this.mainForm.value.al_name = val.al_name;
                this.mainForm.value.al_code = val.al_code;
            })
        );
        this.stepTwo.subscribe(form =>
            form.valueChanges.subscribe(val => {
                console.log(val);
                this.mainForm.value.hq_airport = val.hq_airport;
            })
        );
    }

    stepReady(form: FormGroup, part) {
        switch (part) {
            case 'one': { this.stepOneSource.next(form); } break;
            case 'two': { this.stepTwoSource.next(form); } break;
        }
    }
}
