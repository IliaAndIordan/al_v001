import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// Services
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import {
  CountryStateModel, AirportModel, CountryModel,
  ResponseCountryRegionListModel, ICountryStateModel
} from 'src/app/@core/api/airport/dto';
import { AirportService } from 'src/app/@core/api/airport/airport.service';
import { AmsStorageKeysLocal } from 'src/app/@core/const/ams-storage-key.const';
import { AirlineModel } from 'src/app/@core/auth/api/dto';
// Constants


@Component({
  selector: 'app-edit-airline-dialog',
  templateUrl: './edit-airline.dialog.html',
  styleUrls: ['./edit-airline.dialog.scss']
})

export class EditAirlineDialogComponent implements OnInit {

  form: FormGroup;
  hasSpinner = false;

  airport: AirportModel;
  airline: AirlineModel;
  countryStateList: Array<CountryStateModel>;

  errorMessage: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private apService: AirportService,
    private currUserService: CurrentUserService,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<AirlineModel>,
    @Inject(MAT_DIALOG_DATA) public data: AirlineModel) {
    this.airline = data;
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      alName: new FormControl(this.airline.name, [Validators.required, Validators.minLength(5)]),
      code: new FormControl(this.airline.code, [Validators.required, Validators.minLength(2), Validators.max(2)]),
      description: new FormControl(this.airline.description),
    });
  }

  get alName() { return this.form.get('alName'); }
  get code() { return this.form.get('code'); }
  get description() { return this.form.get('description'); }

  get errorCode(): string {
    return this.code.hasError('required') ? 'Airline code is required.' :
      this.code.hasError('minlength') ? 'Airline code must be 2 characters long' : '';
  }

  get errorAlName(): string {
    return this.alName.hasError('required') ? 'Airline name is required.' :
      this.alName.hasError('minlength') ? 'Airline name must be at least 5 characters long' : '';
  }

  onNoClick(): void {
    // this.dialogRef.close();
  }

  onLoginClick() {

    if (this.form.valid) {
      this.errorMessage = undefined;
      this.hasSpinner = true;

      this.airline.name = this.alName.value;
      this.airline.code = this.code.value;
      this.airline.description = this.description.value;

      console.log('updated airline:', this.airline);

      setTimeout((form: any = this) => {
        form.errorMessage = 'Airline Update Not Impllemented. ';
        form.hasSpinner = false;
      }, 300);

      /*
      this.apService.saveAirport(this.airport)
        .subscribe(resAl => {
          console.log('saveAirport', resAl);
          if (resAl && resAl.status === 'success') {

            this.toastr.success('Airport Updated', 'Airport Updated!');
            this.hasSpinner = false;
            this.dialogRef.close(resAl.data.iAirportModel);
          } else {
            this.errorMessage = 'Airline Update Failed. ' + resAl.message;
            this.hasSpinner = false;
            setTimeout((router: Router) => {
              this.errorMessage = undefined;
            }, 2000);
            // this.spinerService.display(false);
            this.toastr.error('Airline Update Failed', resAl.message);
          }

        });

        */
    } else {
      this.errorMessage = 'Not valid input';
      this.toastr.error('Please enter valid values for fields', 'Not valid input');
    }
  }



  public get country(): CountryModel {
    const data = localStorage.getItem(AmsStorageKeysLocal.Country);
    if (data) {
      const parsedData: CountryModel = <CountryModel>JSON.parse(data);
      if (parsedData) {
        const req = Object.assign(new CountryModel(), parsedData);
        return req;
      }
    }
  }


  loadCountryStateList(): void {

    this.apService.getCountryRegionList(this.country.id)
      .subscribe((responce: ResponseCountryRegionListModel) => {
        // console.log('loadCountryStateist-> responce', responce);

        if (responce.data.countyStateList) {
          this.countryStateList = new Array<CountryStateModel>();
          responce.data.countyStateList.forEach((element: ICountryStateModel) => {
            const tmp: CountryStateModel = CountryStateModel.fromJSON(element);
            this.countryStateList.push(tmp);
          });
        }

        console.log('loadCountryStateist-> countyStateList', this.countryStateList);
      });
  }

}
