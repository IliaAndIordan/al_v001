import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// ---
// Local Components
import { AppRoutes } from '../@core/const/app-routes.const';
import { PublicComponent } from './public.component';
import { HomeComponent } from './home.component';


const routes: Routes = [
    { path: '', component: HomeComponent, },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PublicRoutingModule { }
export const RoutedComponents = [PublicComponent, HomeComponent];
