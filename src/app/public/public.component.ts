import { Component, OnInit } from '@angular/core';

@Component({
    template: '<router-outlet></router-outlet>',
})

export class PublicComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
