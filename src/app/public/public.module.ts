import { NgModule } from '@angular/core';
import { GoogleChartsModule } from 'angular-google-charts';
// Local Modules
import { SharedModule } from '../@share/shared.module';
import { PublicRoutingModule, RoutedComponents } from './routing.module';
import { CommonModule } from '@angular/common';
import { GMAP_API_KEY } from '../@core/const/ams-storage-key.const';
import { PageHeaderComponent } from './page-header/page-header.component';

@NgModule({
    imports: [
        CommonModule,
        PublicRoutingModule,
        SharedModule,
        GoogleChartsModule.forRoot(GMAP_API_KEY),
    ],
    declarations: [
        RoutedComponents,
        PageHeaderComponent,
    ],
    exports: [
    ],
    entryComponents: []
})
export class PublicModule { }

