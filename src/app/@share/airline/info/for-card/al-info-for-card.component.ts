import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { AirportModel } from 'src/app/@core/api/airport/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { Router } from '@angular/router';
import { AirportService } from 'src/app/@core/api/airport/airport.service';
import { IMG_ICAO_URL, IMG_IATA_URL, IMG_WIKI_URL, IMG_OUAP_URL } from 'src/app/@core/const/ams-storage-key.const';
import { AirlineModel } from 'src/app/@core/auth/api/dto';

@Component({
    selector: 'app-al-info-for-card',
    templateUrl: './al-info-for-card.component.html',
})

export class AlInfoForCardComponent implements OnInit, OnDestroy {


    @Input() hqAirport: AirportModel;
    @Input() airline: AirlineModel;

    icaoUrl = IMG_ICAO_URL;
    iataUrl = IMG_IATA_URL;
    wikiUrl = IMG_WIKI_URL;
    ourApUrl = IMG_OUAP_URL;
    constructor(
        private cus: CurrentUserService,
        private router: Router,
        private apService: AirportService) { }

    ngOnDestroy(): void {

    }
    ngOnInit(): void {

    }

}

