import { Pipe, PipeTransform, LOCALE_ID, Inject } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { AmsStorageKeysLocal } from 'src/app/@core/const/ams-storage-key.const';
import { LocalUserSettings } from 'src/app/@core/auth/local-user-settings';


@Pipe({
    name: 'costph'
})
export class CostPerHourPipe implements PipeTransform {

    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {

        if (isNaN(value)) { return null; }
        if (value === null) { return null; }
        if (value === 0) { return null; }

        const argStr = sessionStorage.getItem(AmsStorageKeysLocal.LocalUserSettings);
        let lp100kmTmpg = 1;
        let label = '$/h';
        if (argStr) {
            const settings = Object.assign(new LocalUserSettings(), JSON.parse(argStr));
            if (settings) {
                lp100kmTmpg = settings.displayDistanceMl ? 1 : 1;
                label = settings.displayDistanceMl ? '$/h' : '$/h';
            }
        }
        const num = value * lp100kmTmpg;
        const rv = new DecimalPipe(this.locale).transform(num, '1.2-2') + ' ' + label;
        return rv;
    }
}
