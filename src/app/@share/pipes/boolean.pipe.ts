import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'bool'
})
export class BooleanPipe implements PipeTransform {
    transform(value: boolean): string {

        const rvt = '✔';
        const rvf = '✗';

        if (value) {
            return rvt;
        } else {
            return rvf;
        }
    }
}
