import { Pipe, PipeTransform, LOCALE_ID, Inject } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { AmsStorageKeysLocal } from 'src/app/@core/const/ams-storage-key.const';
import { LocalUserSettings } from 'src/app/@core/auth/local-user-settings';


@Pipe({
    name: 'range'
})
export class RangePipe implements PipeTransform {

    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {

        if (isNaN(value)) { return null; }
        if (value === null) { return null; }
        if (value === 0) { return null; }

        const argStr = sessionStorage.getItem(AmsStorageKeysLocal.LocalUserSettings);
        let kmToMi = 0.539957;
        let label = 'nm';
        if (argStr) {
            const settings = Object.assign(new LocalUserSettings(), JSON.parse(argStr));
            if (settings) {
                kmToMi = settings.displayDistanceMl ? 0.539957 : 1;
                label = settings.displayDistanceMl ? 'nm' : 'km';
            }
        }
        const num = value * kmToMi;
        const rv = new DecimalPipe(this.locale).transform(num, '1.0-0') + ' ' + label;
        return rv;
    }
}
