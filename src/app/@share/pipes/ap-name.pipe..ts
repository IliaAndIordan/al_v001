import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'apname'
})
export class ApNamePipe implements PipeTransform {
    transform(value: number): string {

        const rvt = '✔';
        const rvf = '✗';

        if (value) {
            return rvt;
        } else {
            return rvf;
        }
    }
}
