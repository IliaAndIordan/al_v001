import { Pipe, PipeTransform, LOCALE_ID, Inject } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { AmsStorageKeysLocal } from 'src/app/@core/const/ams-storage-key.const';
import { LocalUserSettings } from 'src/app/@core/auth/local-user-settings';


@Pipe({
    name: 'speed'
})
export class SpeedPipe implements PipeTransform {

    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {

        if (isNaN(value)) { return null; }
        if (value === null) { return null; }
        if (value === 0) { return null; }

        const argStr = sessionStorage.getItem(AmsStorageKeysLocal.LocalUserSettings);
        let kmphToKnot = 0.539957;
        let label = 'Knots';
        if (argStr) {
            const settings = Object.assign(new LocalUserSettings(), JSON.parse(argStr));
            if (settings) {
                kmphToKnot = settings.displayDistanceMl ? 0.539957 : 0.539957;
                label = 'Knots'; // settings.displayDistanceMl ? 'Knots' : 'km/h';
            }
        }
        const num = value * kmphToKnot;
        const rv = new DecimalPipe(this.locale).transform(num, '1.0-0') + ' ' + label;
        return rv;
    }
}
