import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';  // <-- #1 import module

import { MaterialModule } from './material/material.module';
import { ToastrModule } from 'ngx-toastr';
import { ChartsModule } from 'ng2-charts';
import { ModalModule } from './modal/modal.module';
import { ApInfoBaseComponent } from './airport/info/base/ap-info-base.component';
import { ApInfoLineComponent } from './airport/info/line/airport-info-line.component';
import { AlInfoForCardComponent } from './airline/info/for-card/al-info-for-card.component';
import { AcModule } from './aircraft/ac.module';


@NgModule({

    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        ChartsModule,
        ToastrModule.forRoot(),
        // Custom Modules
        MaterialModule,
        ModalModule,
        AcModule,

    ],
    declarations: [
        ApInfoBaseComponent,
        ApInfoLineComponent,
        // Airline
        AlInfoForCardComponent,
    ],
    exports: [
        MaterialModule,
        ModalModule,
        ChartsModule,
        AcModule,

        ApInfoBaseComponent,
        ApInfoLineComponent,
        // Airline
        AlInfoForCardComponent,

    ],
    entryComponents: [
    ]
})

export class SharedModule {

    constructor() { }
}

