import { NgModule, Optional, SkipSelf } from '@angular/core';
import { LoginModalDialog } from './login/login-modal.dialog';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatProgressBarModule } from '@angular/material';
@NgModule({
    declarations: [
        LoginModalDialog,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
    ],
    exports: [
        LoginModalDialog,
    ],
    entryComponents: [
        LoginModalDialog,
    ]
})

export class ModalModule {

    constructor() { }
}
