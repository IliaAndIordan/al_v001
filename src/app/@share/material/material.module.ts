import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule, MatCheckboxModule, MatToolbarModule,
  MatSidenavModule, MatIconModule, MatListModule, MatFormFieldModule,
  MatInputModule, MatRadioModule, MatMenuModule, MatTreeModule,
  MatCardModule, MatTableModule, MatExpansionModule, MatProgressBarModule,
  MatGridListModule,
  MatGridTile,
  MatProgressSpinnerModule,
  MatTooltipModule,
  MatAutocompleteModule,
  MatTabsModule
} from '@angular/material';
import { NavigationModule } from './navigation/navigation.module';
import { LayoutModule } from '@angular/cdk/layout';


@NgModule({
  imports: [
    CommonModule,
    // ---Material modules
    MatButtonModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatTreeModule,
    MatCardModule,
    MatTableModule,
    MatTabsModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatGridListModule,
    MatTooltipModule,
    LayoutModule,
    MatAutocompleteModule,
    // Local Modules
    NavigationModule,
  ],
  exports: [
    // ---Material modules
    MatButtonModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatTreeModule,
    MatCardModule,
    MatTableModule,
    MatTabsModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    LayoutModule,
    MatAutocompleteModule,
    // Local Modules
    NavigationModule,
  ],
  declarations: [
  ]
})
export class MaterialModule { }
