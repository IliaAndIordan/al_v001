import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { BwtNavToolbarComponent } from './bwt-nav-toolbar/bwt-nav-toolbar.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule,
  MatIconModule, MatListModule, MatDialogModule, MatFormFieldModule,
  MatCheckboxModule, MatInputModule, MatRadioModule, MatTooltipModule, MatProgressSpinnerModule
} from '@angular/material';
import { BwtNavListItemLeftComponent } from './bwt-nav-list-item-left/bwt-nav-list-item-left.component';
import { BwtNacSideLeftProfileComponent } from './bwt-nav-side-left-profile/bwt-nav-side-left-profile.component';
import { BwtNavSideProfileInfoComponent } from './bwt-nav-side-profile-info/bwt-nav-side-profile-info.component';
import { RegisterModalDialog } from './modal/register/register-modal.dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TruncatePipe } from '../../pipes/truncate.pipe';
import { BooleanPipe } from '../../pipes/boolean.pipe';
import { LengthPipe } from '../../pipes/length.pipe';
import { MtowPipe } from '../../pipes/mtow.pipe';
import { AmsSideNavFooterComponent } from './sidenav-footer/sidenav-footer.component';
import { RangePipe } from '../../pipes/range.pipe';
import { SpeedPipe } from '../../pipes/speed.pipe';
import { FuelConsumptionhPipe } from '../../pipes/fuel_consumption.pipe';
import { CostPerHourPipe } from '../../pipes/costph.pipe';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatRadioModule,
    MatFormFieldModule,
    MatDialogModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatProgressSpinnerModule,
  ],
  declarations: [
    BwtNavToolbarComponent,
    BwtNavListItemLeftComponent,
    BwtNacSideLeftProfileComponent,
    BwtNavSideProfileInfoComponent,
    RegisterModalDialog,
    AmsSideNavFooterComponent,
    // Pipes
    TruncatePipe,
    BooleanPipe,
    LengthPipe,
    MtowPipe,
    RangePipe,
    SpeedPipe,
    FuelConsumptionhPipe,
    CostPerHourPipe,
  ],
  exports: [
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    BwtNavToolbarComponent,
    BwtNavListItemLeftComponent,
    BwtNacSideLeftProfileComponent,
    BwtNavSideProfileInfoComponent,
    RegisterModalDialog,
    AmsSideNavFooterComponent,
    // Pipes
    TruncatePipe,
    BooleanPipe,
    LengthPipe,
    MtowPipe,
    RangePipe,
    SpeedPipe,
    FuelConsumptionhPipe,
    CostPerHourPipe,
  ],
  entryComponents: [
    RegisterModalDialog
  ]
})
export class NavigationModule { }
