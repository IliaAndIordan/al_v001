import { Component, OnInit, OnDestroy } from '@angular/core';
import { CurrentUserService } from '../../../../@core/auth/current-user.service';
import { UserModel, AirlineModel } from '../../../../@core/auth/api/dto';
import { LocalUserSettings } from '../../../../@core/auth/local-user-settings';
import { AVATAR_IMAGE_URL } from 'src/app/@core/const/ams-storage-key.const';


@Component({
  selector: 'app-bwt-nav-side-profile-info',
  templateUrl: './bwt-nav-side-profile-info.component.html',
  styleUrls: ['./bwt-nav-side-profile-info.component.scss']
})

export class BwtNavSideProfileInfoComponent implements OnInit, OnDestroy {

  public snavLeftWidth = 4;
  public paddingBottom = 0;
  public maxHeight = 64;
  public paddingTop = 12;
  public heightPx = 64;
  public minHeight = 64;

  airline: AirlineModel;
  airlineChangedSubscription: any;
  user: UserModel;
  userChangedSubscription: any;
  localSettingsChanged: any;

  avatarImageUrl: string;
  airlineName = 'Welcome to AMS!';
  slogan = 'To fly to serve';

  constructor(private cus: CurrentUserService) {

    this.snavLeftWidth = 200; // this.currUserService.localSettings.snavLeftWidth;
    this.avatarImageUrl = AVATAR_IMAGE_URL;
  }

  ngOnInit() {

    this.airline = this.cus.airline;
    this.user = this.cus.user;

    this.airlineChangedSubscription = this.cus.AirlineChanged.subscribe((newAl: AirlineModel) => {
      // console.log('StatesComponent: newCountry...', newCountry);
      this.airline = newAl;
      this.onUserChanged();
    });

    this.userChangedSubscription = this.cus.UserChanged.subscribe((newUser: UserModel) => {
      this.user = newUser;
      this.onUserChanged();
    });

    this.localSettingsChanged = this.cus.LocalSettingsChanged.subscribe((settings: LocalUserSettings) => {
      this.onSettingsChanged();
    });
    this.onUserChanged();
    this.onSettingsChanged();
  }

  ngOnDestroy(): void {
    if (this.airlineChangedSubscription) { this.airlineChangedSubscription.unsubscribe(); }
    if (this.userChangedSubscription) { this.userChangedSubscription.unsubscribe(); }
    if (this.localSettingsChanged) { this.localSettingsChanged.unsubscribe(); }
  }

  onUserChanged() {
    this.avatarImageUrl = this.cus.airlineLogoUrl;

    this.airlineName = 'Welcome';
    this.slogan = '';
    if (this.airline) {
      this.airlineName = this.airline.name;
      this.slogan = this.airline.slogan;
    } else if (this.user) {
      this.airlineName = this.user.name;
      this.slogan = 'To fly to serve';
    }

  }

  onSettingsChanged() {
    this.snavLeftWidth = 200; // this.currUserService.localSettings.snavLeftWidth;
    this.paddingBottom = this.snavLeftWidth > 6 ? 64 : 0;
    this.maxHeight = this.snavLeftWidth > 6 ? 136 : 64;
    this.paddingTop = this.snavLeftWidth > 6 ? 20 : 12;
    this.heightPx = this.snavLeftWidth > 6 ? 136 : 64;
    this.minHeight = this.snavLeftWidth > 6 ? 136 : 64;

  }

}
