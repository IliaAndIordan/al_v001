import { Component, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Subject, Subscribable } from 'rxjs';
// Services and Models
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { AirportModel } from 'src/app/@core/api/airport/dto';
import { AirportDataService } from 'src/app/@core/api/airport/airport-data.service';

@Component({
    selector: 'app-ams-sidenav-footer',
    templateUrl: './sidenav-footer.component.html',
})
export class AmsSideNavFooterComponent implements OnInit, OnDestroy {

    @Output() flight: EventEmitter<any> = new EventEmitter<any>();


    private userChangedSubscription: any;
    isAuthenticated = false;

    hqAirport: AirportModel;
    hqAirportChanged: any;

    timeHqAirport: Date;
    curTimeInt: any;

    constructor(
        private breakpointObserver: BreakpointObserver,
        private currUserService: CurrentUserService,
        private apDs: AirportDataService) {
        this.isAuthenticated = this.currUserService.isAuthenticated;

    }


    ngOnInit(): void {
        this.isAuthenticated = this.currUserService.isAuthenticated;
        this.hqAirport = this.currUserService.hqAirport;

        this.hqAirportChanged = this.currUserService.hqAirportSubject
            .subscribe((airport: AirportModel) => {
                this.hqAirport = this.currUserService.hqAirport;
            });

        this.userChangedSubscription = this.currUserService.UserChanged
            .subscribe((newUser: UserModel) => {
                this.onUserChanged(newUser);
            });
        this.timeHqAirportInit();
    }

    timeHqAirportInit(): void {
        //  get current date time, you can set your own Date() over here
        this.timeHqAirport = this.getHqAirportTime();
        // clear Interval, it's my system requirement
        if (this.curTimeInt) {
            clearInterval(this.curTimeInt);
        }

        // set clock and Interval
        this.curTimeInt = setInterval(() => {
            // stationdate = new Date(stationdate.setSeconds(stationdate.getSeconds() + 1));
            this.timeHqAirport = this.getHqAirportTime();
        }, 1000);
    }

    getHqAirportTime(): Date {
        const localDate = new Date();
        const utcMs = localDate.getTime() + (localDate.getTimezoneOffset() * 60000);
        let hqTime = new Date();
        // console.log('uAtc: ', this.hqAirport);
        if (this.hqAirport && this.hqAirport.utc) {
            // console.log('utc: ', this.hqAirport.utc);
            let utc = this.hqAirport.utc;
            if (this.currUserService.isDaylightSavingTime) {
                utc = utc + 1;
            }
            hqTime = new Date(utcMs + (utc * 3600000));
        }
        // console.log('getHqAirportTime: ', hqTime);

        return hqTime;
    }

    onUserChanged(newUser: UserModel) {
        this.isAuthenticated = this.currUserService.isAuthenticated;

        if (this.currUserService.airline && this.currUserService.airline.hq_airport_id > 0) {
            this.apDs.airportById(this.currUserService.airline.hq_airport_id)
                .subscribe((airport: AirportModel) => {
                    this.currUserService.hqAirport = airport;
                    console.log('loadAirport->', airport);
                    this.hqAirport = airport;
                    this.timeHqAirportInit();
                });

        }

    }


    ngOnDestroy(): void {
        if (this.userChangedSubscription) { this.userChangedSubscription.unsubscribe(); }
        if (this.hqAirportChanged) { this.hqAirportChanged.unsubscribe(); }
        if (this.curTimeInt) {
            clearInterval(this.curTimeInt);
        }
    }


}
