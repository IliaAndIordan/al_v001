import { Component, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatSidenav } from '@angular/material';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { AuthService } from 'src/app/@core/auth/api/auth.service';
import { AmsNavSiteLeft } from 'src/app/@core/const/ams-wad-lnav.const';

@Component({
  selector: 'app-bwt-nav-list-item-left',
  templateUrl: './bwt-nav-list-item-left.component.html',
  styleUrls: ['./bwt-nav-list-item-left.component.scss'],
})
export class BwtNavListItemLeftComponent {

  // @ViewChild('drawer') drawer: MatSidenav;
  public navigation = AmsNavSiteLeft;
  public user: UserModel;
  public isAuthenticated: boolean;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
    private router: Router,
    private route: ActivatedRoute,
    private cuService: CurrentUserService,
    private authService: AuthService) {
    // console.log('BwtNavSideLeftComponent ->');
    this.user = this.cuService.user;
    this.isAuthenticated = this.cuService.isAuthenticated;

    this.cuService.UserChanged.subscribe((user: UserModel) => {
      this.onUserChanged();
    });
  }


  public onUserChanged(): void {
    this.user = this.cuService.user;
    this.isAuthenticated = this.cuService.isAuthenticated;
    this.navigation = AmsNavSiteLeft.filter(x => x.userRole <= (this.user ? this.user.role : -1));
    // console.log('app-ams-lnav-list-items.onUserChanged: isAuthenticated=', this.isAuthenticated);
  }

  public isVisible(item) {
    let visible = true;
    if (item.authorised !== 'undefined') {
      visible = (item.authorised === this.cuService.isAuthenticated);
    }

    return visible;
  }


  logout(): void {
    this.authService.logout();
  }


}
