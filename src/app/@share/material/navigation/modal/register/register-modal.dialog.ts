import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, ErrorStateMatcher } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
// Services
import { AuthService } from 'src/app/@core/auth/api/auth.service';
import { ResponseAuthenticate } from 'src/app/@core/auth/api/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
// Constants

export interface LoginData {
  email: string;
  password: string;
}

export class EMailStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}


@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.dialog.html',
  styleUrls: ['./register-modal.dialog.scss']
})


// tslint:disable-next-line:component-class-suffix
export class RegisterModalDialog implements OnInit {

  form: FormGroup;

  email: string;
  password: string;
  hasSpinner = false;
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required,
  ]);
  emailMatcher = new EMailStateMatcher();
  errorMessage: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<RegisterModalDialog>,
    @Inject(MAT_DIALOG_DATA) public data: LoginData) {
    this.email = data.email;
    this.password = data.password;
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: this.emailFormControl,
      password: this.passwordFormControl
    });
  }

  onNoClick(): void {
    // this.dialogRef.close();
  }

  onLoginClick() {

    if (this.form.valid) {
      this.errorMessage = undefined;
      this.hasSpinner = true;
      this.authService.register(
        this.form.value['email'], this.form.value['password'])
        .subscribe((resAuth: ResponseAuthenticate) => {
          console.log('resAuth ->', resAuth);
          if (resAuth && resAuth.status === 'success') {
            // this.spinerService.display(false);

            this.toastr.success('Welcome ' + resAuth.data.current_user.name + '!', 'Login success');
            this.hasSpinner = false;
            setTimeout((router: Router) => {
              if (!resAuth.data.airline) {
                this.router.navigate(['/', AppRoutes.Airline]);
              } else {
                this.router.navigate(['/', AppRoutes.Dashboard]);
              }

            }, 300);
            this.dialogRef.close(resAuth.data.current_user.name);
          } else {
            this.errorMessage = 'Registration Failed. ' + resAuth.message;
            this.hasSpinner = false;
            setTimeout((router: Router) => {
              this.errorMessage = undefined;
            }, 2000);
            // this.spinerService.display(false);
            this.toastr.error('Registration Failed', resAuth.message);
          }
        });
    } else {
      this.toastr.info('Please enter valid values for fields', 'Not valid input');
    }
  }


}
