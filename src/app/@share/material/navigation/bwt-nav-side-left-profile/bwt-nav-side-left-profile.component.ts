import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { CurrentUserService } from '../../../../@core/auth/current-user.service';
import { UserModel, AirlineModel } from '../../../../@core/auth/api/dto';
import { AVATAR_IMAGE_URL } from 'src/app/@core/const/ams-storage-key.const';

@Component({
  selector: 'app-bwt-nav-side-left-profile',
  templateUrl: './bwt-nav-side-left-profile.component.html',
  styleUrls: ['./bwt-nav-side-left-profile.component.scss']
})
export class BwtNacSideLeftProfileComponent implements OnInit, OnDestroy {

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onClick: EventEmitter<any> = new EventEmitter<any>();

  airline: AirlineModel;
  airlineChangedSubscription: any;
  user: UserModel;
  userChangedSubscription: any;

  avatarImageUrl: string;
  title: string;


  constructor(private cus: CurrentUserService) {
    this.avatarImageUrl = AVATAR_IMAGE_URL;
    this.title = 'Login';
  }

  ngOnInit() {
    this.airline = this.cus.airline;
    this.airlineChangedSubscription = this.cus.AirlineChanged.subscribe((newAl: AirlineModel) => {
      // console.log('StatesComponent: newCountry...', newCountry);
      this.airline = newAl;
      this.user = this.cus.user;
      this.onUserChanged();
    });
    this.user = this.cus.user;
    this.userChangedSubscription = this.cus.UserChanged.subscribe((user: UserModel) => {
      this.user = user;
      this.airline = this.cus.airline;
      this.onUserChanged();
    });

    this.onUserChanged();
  }

  ngOnDestroy(): void {
    if (this.airlineChangedSubscription) { this.airlineChangedSubscription.unsubscribe(); }
    if (this.userChangedSubscription) { this.userChangedSubscription.unsubscribe(); }
  }

  onUserChanged() {

    if (this.user) {
      this.title = this.user.name ? this.user.name : this.user.eMail;
      this.avatarImageUrl = this.cus.avatarUrl;
    } else {
      this.user = undefined;
      this.title = 'Login';
      this.avatarImageUrl = AVATAR_IMAGE_URL;
    }

    if (this.airline) {
    } else {
      this.airline = undefined;
    }

  }

  handleClick(event: any) {
    // what we sending to the event handler that is being passed to parent function
    this.onClick.emit('emit');
  }

}
