import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { RegisterModalDialog } from '../modal/register/register-modal.dialog';
import { AmsApplications } from 'src/app/@core/const/ams-application.const';

@Component({
  selector: 'app-bwt-nav-toolbar',
  templateUrl: './bwt-nav-toolbar.component.html',
  styleUrls: ['./bwt-nav-toolbar.component.scss'],
})
export class BwtNavToolbarComponent {


  @Output() sideLeftToggle: EventEmitter<any> = new EventEmitter<any>();
  @Output() sideRightToggle: EventEmitter<any> = new EventEmitter<any>();

  // @Output() snavLeftTogggle: EventEmitter<any> = new EventEmitter<any>();

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  private userChangedSubscription: any;

  isAuthenticated = false;
  appl = AmsApplications.AMS;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private currUserService: CurrentUserService,
    public dialogService: MatDialog) {
    this.isAuthenticated = this.currUserService.isAuthenticated;
    this.userChangedSubscription = this.currUserService.UserChanged
      .subscribe(newUser => {
        this.isAuthenticated = this.currUserService.isAuthenticated;
      });
  }

  leftToggle() {
    console.log('leftToggle ->');
    this.sideLeftToggle.emit();
  }

  rightToggle() {
    console.log('rightToggle ->');
    this.sideRightToggle.emit();
  }


  public registerDialogShow(): void {
    const dialogRef = this.dialogService.open(RegisterModalDialog, {
      width: '500px',
      height: '320px',
      data: { email: undefined, password: undefined }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      // this.password = result.password;
    });
  }
}
