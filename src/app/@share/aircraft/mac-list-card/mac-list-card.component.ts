import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatCardSmImage } from '@angular/material';
import { AcMacModel } from 'src/app/@core/api/aircraft/dto';
const Preload = 'simple-logo-card-image-preload';
const Wide = 'simple-logo-card-image-wide';
const Tall = 'simple-logo-card-image-tall';


@Component({
  selector: 'app-mac-list-card',
  templateUrl: './mac-list-card.component.html',
})
export class MacListCardComponent implements OnInit {

  /**
  * CONSTRUCTOR
  */
  constructor() { }

  /**
   * BINDINGS
   */
  @Input() mac: AcMacModel;
  @Input() logo: string;
  @Output() cardClicked: EventEmitter<any> = new EventEmitter<any>();

  /**
   * FIELDS
   */
  imageClass: string = Preload;

  ngOnInit() { }

  onImageLoad() {
    this.imageClass = this.getImageClass();
  }

  onImageError() {
    this.logo = null; // Don't show broken image.
  }

  getImageClass() {
    const image = new Image();
    image.src = this.logo;
    if (image.width > image.height + 20 || image.width === image.height) {
      // return wide image class
      return Wide;
    } else {
      return Tall;
    }
  }

  closeCard() {

  }

  onClick() {
    this.cardClicked.emit(this.mac.mac_id);
  }

}

