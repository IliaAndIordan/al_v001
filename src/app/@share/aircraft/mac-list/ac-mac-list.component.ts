import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
// Constants
import { IMG_WIKI_URL, IMG_OUAP_URL } from 'src/app/@core/const/ams-storage-key.const';
// Services
import { UserModel } from 'src/app/@core/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { AuthService } from 'src/app/@core/auth/api/auth.service';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AcMacModel } from 'src/app/@core/api/aircraft/dto';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';

@Component({
    selector: 'app-ac-mac-list',
    templateUrl: './ac-mac-list.component.html',
})


export class AcMacListComponent implements OnInit, OnDestroy {

    @Input() macList: Array<AcMacModel>;
    @Output() refreshListClick: EventEmitter<any> = new EventEmitter<any>();

    wikiUrl = IMG_WIKI_URL;
    ourApUrl = IMG_OUAP_URL;

    user: UserModel;
    userChanged: any;
    isAdmin: boolean;

    colspan3Card = 4;


    constructor(
        private router: Router,
        private breakpointObserver: BreakpointObserver,
        private cus: CurrentUserService,
        private authService: AuthService,
    ) { }



    ngOnInit(): void {
        this.user = this.cus.user;
        this.isAdmin = this.cus.isAdmin;
        this.breakpointObserverInit();
        this.userChanged = this.cus.UserChanged.subscribe(newUser => {
            console.log('newUser->', newUser);
            this.user = newUser;
            this.isAdmin = this.cus.isAdmin;
        });
    }

    breakpointObserverInit(): void {
        this.breakpointObserver
            .observe([Breakpoints.Handset])
            .subscribe((state: BreakpointState) => {
                if (state.matches) {
                    console.log('Matches Handset viewport  mode');
                    this.colspan3Card = 12;
                }
            });
        this.breakpointObserver
            .observe([Breakpoints.Tablet])
            .subscribe((state: BreakpointState) => {
                if (state.matches) {
                    console.log('Matches Tablet viewport mode');
                    this.colspan3Card = 6;
                }
            });

        this.breakpointObserver
            .observe([Breakpoints.Web])
            .subscribe((state: BreakpointState) => {
                if (state.matches) {
                    console.log('Matches Web viewport mode');
                    this.colspan3Card = 4;
                }
            });
        this.breakpointObserver
            .observe([Breakpoints.Small])
            .subscribe((state: BreakpointState) => {
                if (state.matches) {
                    console.log('Matches Handset viewport  mode');
                    this.colspan3Card = 12;
                }
            });

    }


    ngOnDestroy(): void {
        if (this.userChanged) {
            this.userChanged.unsubscribe();
        }
    }

    onRefreshListClick() {
        this.refreshListClick.emit();
    }

    //#region Navigation

    gotoAircraftAdmin() {
        if (this.cus.user && this.cus.isAdmin) {
            this.router.navigate(['/', AppRoutes.Aircrafts]);
        } else {
            this.logout();
        }

    }

    logout(): void {
        this.authService.logout();
    }


    //#endregion

    //#region DATA

    onAcMacClick(macId: number): void {
        console.log('onAcMacClick-> macId=', macId);
    }

    //#endregion


}
