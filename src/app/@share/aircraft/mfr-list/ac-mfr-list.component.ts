import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
// Constants
import { IMG_WIKI_URL, IMG_OUAP_URL } from 'src/app/@core/const/ams-storage-key.const';
// Services
import { UserModel } from 'src/app/@core/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { AuthService } from 'src/app/@core/auth/api/auth.service';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AcMfrModel } from 'src/app/@core/api/aircraft/dto';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { AcMfrService } from 'src/app/@core/api/aircraft/ac-mfr.service';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'app-ac-mfr-list',
    templateUrl: './ac-mfr-list.component.html',
})


export class AcMfrListComponent implements OnInit, OnDestroy {

    @Input() manufacturers: Array<AcMfrModel>;
    @Output() refreshListClick: EventEmitter<any> = new EventEmitter<any>();

    wikiUrl = IMG_WIKI_URL;
    ourApUrl = IMG_OUAP_URL;

    user: UserModel;
    userChanged: any;
    isAdmin: boolean;

    colspan3Card = 4;


    constructor(
        private router: Router,
        private toastr: ToastrService,
        private breakpointObserver: BreakpointObserver,
        private cus: CurrentUserService,
        private authService: AuthService,
        private acMfrService: AcMfrService,
    ) { }



    ngOnInit(): void {
        this.user = this.cus.user;
        this.isAdmin = this.cus.isAdmin;
        this.userChanged = this.cus.UserChanged.subscribe(newUser => {
            console.log('newUser->', newUser);
            this.user = newUser;
            this.isAdmin = this.cus.isAdmin;
        });
    }

    breakpointObserverInit(): void {
        this.breakpointObserver
            .observe([Breakpoints.Handset])
            .subscribe((state: BreakpointState) => {
                if (state.matches) {
                    console.log('Matches Handset viewport  mode');
                    this.colspan3Card = 12;
                }
            });
        this.breakpointObserver
            .observe([Breakpoints.Tablet])
            .subscribe((state: BreakpointState) => {
                if (state.matches) {
                    console.log('Matches Tablet viewport mode');
                    this.colspan3Card = 6;
                }
            });

        this.breakpointObserver
            .observe([Breakpoints.Web])
            .subscribe((state: BreakpointState) => {
                if (state.matches) {
                    console.log('Matches Web viewport mode');
                    this.colspan3Card = 4;
                }
            });
        this.breakpointObserver
            .observe([Breakpoints.Small])
            .subscribe((state: BreakpointState) => {
                if (state.matches) {
                    console.log('Matches Handset viewport  mode');
                    this.colspan3Card = 12;
                }
            });

    }


    ngOnDestroy(): void {
        if (this.userChanged) {
            this.userChanged.unsubscribe();
        }
    }

    onRefreshListClick() {
        this.refreshListClick.emit();
    }

    //#region Navigation

    gotoAircraftAdmin() {
        if (this.cus.user && this.cus.isAdmin) {
            this.router.navigate(['/', AppRoutes.Aircrafts]);
        } else {
            this.logout();
        }

    }

    logout(): void {
        this.authService.logout();
    }


    //#endregion

    //#region DATA

    onAcMfrClick(mfr_id: number): void {
        console.log('onAcMfrClick-> mfr_id=', mfr_id);
        const mfrSelected = this.manufacturers.filter(x => x.mfr_id === mfr_id);
        if (mfrSelected && mfrSelected.length > 0) {
            console.log('onAcMfrClick-> mfrSelected=', mfrSelected);
            const mfr = mfrSelected[0];
            this.acMfrService.currMfr = mfr;
            console.log('onAcMfrClick-> mfr=', mfr);
            this.router.navigate(['/' + AppRoutes.Manufacturers + '/mfr']);
        } else {
            let msg = 'Can nt find aircraft manufacturer with PIK ' + mfr_id;
            msg += ' Please, select a manufacturer from list to proceed.';
            this.toastr.error(msg, 'Navigation Failed.');
        }

    }

    //#endregion


}
