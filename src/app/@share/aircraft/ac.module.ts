import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import { ToastrModule } from 'ngx-toastr';
import {
    MatButtonModule, MatMenuModule, MatIconModule, MatListModule,
    MatCardModule, MatTableModule, MatProgressBarModule, MatGridListModule,
    MatProgressSpinnerModule, MatTooltipModule, MatAutocompleteModule, MatTabsModule
} from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
// Components
import { AcMfrListComponent } from './mfr-list/ac-mfr-list.component';
import { MfrLogoCardComponent } from './mfr-logo-card/mfr-logo-card.component';
import { AcMacListComponent } from './mac-list/ac-mac-list.component';
import { MacListCardComponent } from './mac-list-card/mac-list-card.component';
import { RangePipe } from '../pipes/range.pipe';
import { MaterialModule } from '../material/material.module';
import { ManufacturerHeadCardComponent } from './manufacturer/card/mfr-head-card.component';
import { AcMfrMacListComponent } from './mfr-mac-list/mfr-mac-list.component';
import { AcMfrMacListCardComponent } from './mfr-mac-list/card/mfr-mac-list-card.component';

@NgModule({

    imports: [
        CommonModule,
        RouterModule,
        ChartsModule,
        ToastrModule.forRoot(),
        // Material
        MaterialModule,
        MatButtonModule,
        MatMenuModule,
        MatIconModule,
        MatListModule,
        MatCardModule,
        MatTableModule,
        MatProgressBarModule,
        MatGridListModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        MatTabsModule,
        LayoutModule,
        MatAutocompleteModule,

    ],
    declarations: [
        MfrLogoCardComponent,
        ManufacturerHeadCardComponent,
        AcMfrListComponent,
        AcMacListComponent,
        MacListCardComponent,
        AcMfrMacListComponent,
        AcMfrMacListCardComponent,
    ],
    exports: [
        MfrLogoCardComponent,
        AcMfrListComponent,
        AcMacListComponent,
        MacListCardComponent,
        ManufacturerHeadCardComponent,
        AcMfrMacListComponent,
        AcMfrMacListCardComponent,
    ],
    entryComponents: [
    ]
})
export class AcModule {

    constructor() { }
}
