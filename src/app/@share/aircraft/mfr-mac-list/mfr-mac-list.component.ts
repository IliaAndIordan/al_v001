import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
// Constants
import { IMG_WIKI_URL, IMG_OUAP_URL } from 'src/app/@core/const/ams-storage-key.const';
// Services
import { UserModel } from 'src/app/@core/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AcMacModel, AcMfrModel } from 'src/app/@core/api/aircraft/dto';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-mfr-mac-list',
    templateUrl: './mfr-mac-list.component.html',
})


export class AcMfrMacListComponent implements OnInit, OnDestroy {

    @Input() mfr: AcMfrModel;
    @Input() macList: Array<AcMacModel>;
    @Output() refreshListClick: EventEmitter<any> = new EventEmitter<any>();

    wikiUrl = IMG_WIKI_URL;
    ourApUrl = IMG_OUAP_URL;

    user: UserModel;
    isAdmin: boolean;

    private manufacturerChanged: any;


    colspan3Card = 4;


    constructor(
        private router: Router,
        private toastr: ToastrService,
        private cus: CurrentUserService,
    ) { }

    ngOnInit(): void {
        this.user = this.cus.user;
        this.isAdmin = this.cus.isAdmin;
    }

    ngOnDestroy(): void {

    }

    onRefreshListClick() {
        this.refreshListClick.emit();
    }

    //#region Navigation

    onMacClick() {
        if (this.cus.user && this.cus.isAdmin) {
            this.router.navigate(['/', AppRoutes.Aircrafts]);
        }

    }

    //#endregion

    //#region DATA
    /*
        onAcMfrClick(mfr_id: number): void {
            console.log('onAcMfrClick-> mfr_id=', mfr_id);
            const mfrSelected = this.manufacturers.filter(x => x.mfr_id === mfr_id);
            if (mfrSelected && mfrSelected.length > 0) {
                console.log('onAcMfrClick-> mfrSelected=', mfrSelected);
                const mfr = mfrSelected[0];
                this.acMfrService.currMfr = mfr;
                console.log('onAcMfrClick-> mfr=', mfr);
                this.router.navigate(['/' + AppRoutes.Manufacturers + '/mfr']);
            } else {
                let msg = 'Can nt find aircraft manufacturer with PIK ' + mfr_id;
                msg += ' Please, select a manufacturer from list to proceed.';
                this.toastr.error(msg, 'Navigation Failed.');
            }

        }
    */
    //#endregion


}
