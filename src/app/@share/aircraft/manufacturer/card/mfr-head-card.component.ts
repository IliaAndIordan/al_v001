import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { AcMfrModel } from 'src/app/@core/api/aircraft/dto';
import { Router } from '@angular/router';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AC_IMAGE_URL, COMMON_IMAGE_URL } from 'src/app/@core/const/ams-storage-key.const';
const Preload = 'simple-logo-card-image-preload';
const Wide = 'simple-logo-card-image-wide';
const Tall = 'simple-logo-card-image-tall';


@Component({
  selector: 'app-mfr-head-card',
  templateUrl: './mfr-head-card.component.html',
})
export class ManufacturerHeadCardComponent implements OnInit {

  isAdmin: boolean;
  /**
  * CONSTRUCTOR
  */
  constructor(private router: Router,
    private cus: CurrentUserService) { }

  /**
   * BINDINGS
   */
  @Input() mfr: AcMfrModel;
  @Input() cover: string;
  @Input() coverbkg: string;
  @Output() cardClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() editClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() selectClicked: EventEmitter<any> = new EventEmitter<any>();

  /**
   * FIELDS
   */
  imageClass: string = Preload;

  ngOnInit() {
    this.isAdmin = this.cus.isAdmin;
    this.cover = this.mfr.logoUrl;
    this.coverbkg = AC_IMAGE_URL + 'mfr_' + this.mfr.mfr_id + '_headquater_01.png';
  }

  onImageLoad() {
    this.imageClass = this.getImageClass();
  }

  onImageError() {
    this.cover = COMMON_IMAGE_URL + 'newspaper-title-bkg-600-160.png'; // Don't show broken image.
  }

  getImageClass() {
    const image = new Image();
    image.src = this.cover;
    if (image.width > image.height + 20 || image.width === image.height) {
      // return wide image class
      return Wide;
    } else {
      return Tall;
    }
  }

  closeCard() {

  }

  onClick() {
    this.cardClicked.emit(this.mfr.mfr_id);
  }

  onEditClicked() {
    this.editClicked.emit(this.mfr);
  }

  onSelectClicked() {
    this.selectClicked.emit(this.mfr);
  }


  gotoManufacturers() {
    this.router.navigate(['/', AppRoutes.Manufacturers]);
  }
}

