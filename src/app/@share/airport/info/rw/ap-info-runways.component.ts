import { OnInit, OnDestroy, Input, Component } from '@angular/core';
import { Router } from '@angular/router';
// Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { AirportRunwayService } from 'src/app/@core/api/airport-runway/airport-runway.service';
// Models
import { AirportRunwayModel } from 'src/app/@core/api/airport-runway/dto';

@Component({
    selector: 'app-ap-info-runways',
    templateUrl: './ap-info-runways.component.html',
})

export class ApInfoRunwaysComponent implements OnInit, OnDestroy {


    @Input() runways: Array<AirportRunwayModel>;

    rw_lenght_tooltip_text = 'A runway of at least 6,000 ft (1,829 m) in length is usually' +
    ' adequate for aircraft weights below approximately 200,000 lb (90,718 kg). Larger aircraft' +
    ' including widebodies will usually require at least 8,000 ft (2,438 m) at sea level and ' +
    ' somewhat more at higher altitude airports';

    constructor(
        private cus: CurrentUserService,
        private rwService: AirportRunwayService,
        private router: Router) { }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

}
