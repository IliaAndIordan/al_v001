import { IDateModel } from './date.model';

export class AirportViewModel {
    airport_id: number;
    iata: string;
    icao: string;
    aname: string;
    lat: number;
    lon: number;
    active: boolean;
    utc: number;
    ap_type_name: string;
    country_iso2: string;
    country_name: string;
    state_iso: string;
    state_name: string;
    city_name: string;
    max_rlenght_m: number;
    max_mtow_kg: number;
    home_link: string;
    wikipedia_link: string;
    notes: string;
    last_updated: Date;
    sname: string;
    ap_type_id: number;
    country_id: number;
    state_id: number;
    city_id: number;
    ams_status_id: number;

    public static fromJSON(json: IAirportViewModel): AirportViewModel {
        const vs = Object.create(AirportViewModel.prototype);
        return Object.assign(vs, json, {
            last_updated: json.last_updated ? Date.parse(json.last_updated) : undefined,
            active: json.active === 0 ? false : true,
            // flagUrl: COMMON_IMAGE_URL + 'flags/' + json.iso2.toLowerCase() + '.png',
            // apActivePct: (json.apActive / json.airports)
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? AirportViewModel.fromJSON(value) : value;
    }

    public toJSON(): IAirportViewModel {
        return Object.assign({}, this, {
            last_updated: this.last_updated instanceof Date ? this.last_updated.toISOString().slice(0, 19).replace('T', ' ') : undefined,
            active: this.active ? 1 : 0,
        });
    }
}

export interface IAirportViewModel {
    airport_id: number;
    iata: string;
    icao: string;
    aname: string;
    lat: number;
    lon: number;
    active: number;
    utc: number;
    ap_type_name: string;
    country_iso2: string;
    country_name: string;
    state_iso: string;
    state_name: string;
    city_name: string;
    max_rlenght_m: number;
    max_mtow_kg: number;
    home_link: string;
    wikipedia_link: string;
    notes: string;
    last_updated: string;
    sname: string;
    ap_type_id: number;
    country_id: number;
    state_id: number;
    city_id: number;
    ams_status_id: number;
}

export class IAirportListModel {
    airports_db_all: Array<IAirportViewModel>;

    getCountryModelList(): Array<AirportViewModel> {
        const rv = new Array<AirportViewModel>();
        if (this.airports_db_all) {
            this.airports_db_all.forEach((element: IAirportViewModel) => {


                const tmp: AirportViewModel = AirportViewModel.fromJSON(element);
                rv.push(tmp);
            });
        }

        return rv;
    }
}

export interface ResponseAirportListModel {
    status: string;
    message: string;
    data: IAirportListModel;
}
