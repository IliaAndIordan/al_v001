import { CountryModel, ICountryStateModel } from '../api/airport/dto';
import { ChartType, ChartOptions } from 'chart.js';

export class PieMiniChartViewModel {
    options: MiniChartOptionModel;
    data: Array<number>;
    labels: Array<string>;
    colors: Array<any>;
    bgm = 'bgm-lightblue';

    public static fromCountryStateModel(countryState: ICountryStateModel): PieMiniChartViewModel {
        const rv = new PieMiniChartViewModel();
        rv.data = new Array<number>();
        rv.data.push(countryState.apActive);
        rv.data.push(countryState.airports - countryState.apActive);
        rv.labels = ['Active', 'Inactive'];
        rv.colors = [
            {
                borderColor: 'rgba(255,255,255,0.1)',
                backgroundColor: ['rgba(255,255,255,0.8)', 'rgba(255,255,255,0.2)'],
            }
        ];
        const options = new MiniChartOptionModel();
        // const offset = ['Active Airports', 'Airports'];
        const offset = ['Active', 'Inactive'];
        options.tooltipValueLookups = { 'offset': offset };
        options.tooltipFormat =  '{{offset:offset}}: {{value}}';
        rv.options = options;
        rv.bgm = 'bgm-bluegray';
        if (countryState.apActive > 0) {
            rv.bgm = 'bgm-lightblue';
            if (countryState.apActive === countryState.airports) {
                rv.bgm = 'bgm-green';
            }

        }
        return rv;
    }

    public static fromCountryModel(cm: CountryModel): PieMiniChartViewModel {
        const rv = new PieMiniChartViewModel();
        rv.data = new Array<number>();
        rv.data.push(cm.apActive);
        rv.data.push(cm.airports);
        rv.labels = ['Active Airports', 'Airports'];
        rv.colors = [
            { // 1st Year.
                backgroundColor: '#fff',
                borderColor: 'rgba(227, 227, 227, 0.20)',
            },
            { // 2nd Year.
                backgroundColor: 'rgba(255,255,255,0.4)',
            },
            { // 2nd Year.
                backgroundColor: 'rgba(255,255,255,0.4)',
            }
            ,
            { // 2nd Year.
                backgroundColor: 'rgba(255,255,255,0.2)',
            }
        ];
        rv.options = new MiniChartOptionModel();
        const tooltipValueJson = ['Active Airports', 'Airports'];
        rv.options.tooltipValueLookups = { 'offset': tooltipValueJson };
        rv.options.tooltipFormat = '{{offset:offset}}: {{value}}';
        rv.bgm = 'bgm-bluegray';
        if (cm.apActive > 0) {
            rv.bgm = 'bgm-lightblue';
            if (cm.apActive === cm.airports) {
                rv.bgm = 'bgm-green';
            }

        }
        return rv;
    }

}
export class MiniChartOptionModel implements  ChartOptions  {
    responsive = true;
    tooltipFormat: any;
    tooltipValueLookups: any;
    width = 45;
    height = 45;
    sliceColors = ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)'];
    offset = 0;
    borderWidth = 0;
}
