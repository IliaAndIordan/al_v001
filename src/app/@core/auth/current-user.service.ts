import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Subject } from 'rxjs/Subject';

// Models
import { UserModel, AirlineModel } from './api/dto';
import { AmsStorageKeysLocal, AVATAR_IMAGE_URL, AL_LOGO_IMAGE_URL } from '../const/ams-storage-key.const';
import { LocalUserSettings } from './local-user-settings';
import { TokenService } from './token.service';
import { GravatarService } from '@infinitycube/gravatar';
import { AuthService } from './api/auth.service';
import { latLng, LatLng } from 'leaflet';
import { AirportModel, IAirportModel } from '../api/airport/dto';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class CurrentUserService {

    public currHubAirportSubject = new BehaviorSubject<AirportModel>(null);
    public hqAirportSubject = new BehaviorSubject<AirportModel>(null);

    constructor(private tockenService: TokenService,
        private gravatarService: GravatarService) { }

    //#region User

    /**
     * User Changed subject
     * subscribe to it if you want to be notified when changes appear
     */
    public UserChanged = new Subject<UserModel>();

    public getLocation(): Observable<LatLng> {
        return new Observable(obs => {
            const str = sessionStorage.getItem(AmsStorageKeysLocal.UserLocation);
            if (!str) {
                navigator.geolocation.getCurrentPosition(
                    pos => {
                        const rv = new LatLng(pos.coords.latitude, pos.coords.longitude);
                        sessionStorage.setItem(AmsStorageKeysLocal.UserLocation, JSON.stringify(rv));
                        obs.next(rv);
                        obs.complete();
                    },
                    error => {
                        console.log('error', error);
                        const rv = new LatLng(42.704198, 23.2840454);
                        obs.next(rv);
                        obs.complete();
                    }
                );
            } else {
                const rv = <LatLng>JSON.parse(str);
                obs.next(rv);
                obs.complete();
            }

        });
    }



    public get user(): UserModel {
        const userStr = sessionStorage.getItem(AmsStorageKeysLocal.CurrentUser);
        let userObj;
        if (userStr) {
            userObj = Object.assign(new UserModel(), JSON.parse(userStr));
        }
        return userObj;
    }

    public set user(userObj: UserModel) {
        if (userObj) {
            sessionStorage.setItem(AmsStorageKeysLocal.CurrentUser, JSON.stringify(userObj));
        } else {
            sessionStorage.removeItem(AmsStorageKeysLocal.CurrentUser);
        }
        this.UserChanged.next(userObj);
    }

    public get isLogged(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        retValue = (userObj && !this.tockenService.isTokenExpired()) ? true : false;
        if (retValue) {
            // console.log('isLogged user: ', userObj);
        }
        // console.log('isLogged: ' + retValue);
        return retValue;
    }

    public get isAdmin(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        retValue = (userObj && !this.tockenService.isTokenExpired()) ? true : false;
        if (retValue) {
            retValue = (userObj.role === 1);
        }
        // console.log('isLogged: ' + retValue);
        return retValue;
    }

    public get isEditor(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        retValue = (userObj && !this.tockenService.isTokenExpired()) ? true : false;
        if (retValue) {
            retValue = (userObj.role === 2);
        }
        // console.log('isLogged: ' + retValue);
        return retValue;
    }

    public get isAuthenticated(): boolean {
        // console.log('isAuthenticated: ');
        return this.isLogged;
    }

    public get avatarUrl(): string {
        let retValue = AVATAR_IMAGE_URL;
        const userObj: UserModel = this.user;

        if (userObj && userObj.eMail) {
            const gravatarImgUrl = this.gravatarService.url(userObj.eMail, 128, 'identicon');
            if (gravatarImgUrl) {
                retValue = gravatarImgUrl;
            }
        }

        return retValue;
    }

    public get airlineLogoUrl(): string {
        let retValue = AL_LOGO_IMAGE_URL;
        const userObj: UserModel = this.user;
        const airline: AirlineModel = this.airline;
        if (airline && airline.logoUrl) {
            retValue = airline.logoUrl;
        } else if (userObj && userObj.eMail) {
            const gravatarImgUrl = this.gravatarService.url(userObj.eMail, 128, 'identicon');
            if (gravatarImgUrl) {
                retValue = gravatarImgUrl;
            }
        }

        return retValue;
    }

    //#endregion

    //#region Airline

    /**
     * Airline Changed subject
     * subscribe to it if you want to be notified when changes appear
     */
    // tslint:disable-next-line:member-ordering
    public AirlineChanged = new Subject<AirlineModel>();

    public set airline(airlineObj: AirlineModel) {
        if (airlineObj && airlineObj !== null) {
            sessionStorage.setItem(AmsStorageKeysLocal.CurrentAirline, JSON.stringify(airlineObj));
        } else {
            sessionStorage.removeItem(AmsStorageKeysLocal.CurrentAirline);
        }
        this.AirlineChanged.next(airlineObj);
    }

    public get airline(): AirlineModel {
        const airlineStr = sessionStorage.getItem(AmsStorageKeysLocal.CurrentAirline);
        let airlineObj;
        if (airlineStr) {
            airlineObj = Object.assign(new AirlineModel(), JSON.parse(airlineStr));
        }
        return airlineObj;
    }

    //#endregion

    //#region Local User Settings

    // tslint:disable-next-line:member-ordering
    public LocalSettingsChanged = new Subject<LocalUserSettings>();

    public get localSettings(): LocalUserSettings {
        const userStr = sessionStorage.getItem(AmsStorageKeysLocal.LocalUserSettings);
        let userObj;
        if (userStr) {
            userObj = Object.assign(new LocalUserSettings(), JSON.parse(userStr));
        } else {
            userObj = new LocalUserSettings();
            userObj.snavLeftWidth = 4;
            sessionStorage.setItem(AmsStorageKeysLocal.LocalUserSettings, JSON.stringify(userObj));
        }
        return userObj;
    }

    public set localSettings(userObj: LocalUserSettings) {
        if (userObj) {
            sessionStorage.setItem(AmsStorageKeysLocal.LocalUserSettings, JSON.stringify(userObj));
        } else {
            sessionStorage.removeItem(AmsStorageKeysLocal.LocalUserSettings);
        }
        this.LocalSettingsChanged.next(userObj);
    }

    //#endregion

    //#region Airports

    public get currHubAirport(): AirportModel {
        let airport: AirportModel;
        const data = localStorage.getItem(AmsStorageKeysLocal.CurrHubAirport);
        if (data) {
            const parsedData: IAirportModel = JSON.parse(data);
            airport = AirportModel.fromJSON(parsedData);
        }

        return airport;
    }

    public set currHubAirport(value: AirportModel) {
        const currVal = this.currHubAirport;
        if (value) {
            localStorage.setItem(AmsStorageKeysLocal.CurrHubAirport, JSON.stringify(value));
        } else {
            // localStorage.removeItem(this._selCountryKey);
        }

        if (currVal !== value) {
            this.currHubAirportSubject.next(value);
        }
    }


    public get hqAirport(): AirportModel {
        let airport: AirportModel;
        const data = localStorage.getItem(AmsStorageKeysLocal.HqAirport);
        if (data) {
            const parsedData: IAirportModel = JSON.parse(data);
            airport = AirportModel.fromJSON(parsedData);
        }

        return airport;
    }

    public set hqAirport(value: AirportModel) {
        const currVal = this.hqAirport;
        if (value) {
            localStorage.setItem(AmsStorageKeysLocal.HqAirport, JSON.stringify(value));
        } else {
            // localStorage.removeItem(this._selCountryKey);
        }

        if (currVal !== value) {
            this.hqAirportSubject.next(value);
        }
    }
    //#endregion

    //#region DateTime

    private maxTimezoneOffset(): number {
        const jan = new Date(new Date().getFullYear(), 0, 1);
        const jul = new Date(new Date().getFullYear(), 6, 1);
        return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
    }

    get isDaylightSavingTime(): boolean {
        return new Date().getTimezoneOffset() < this.maxTimezoneOffset();
    }

    //#endregion
}
