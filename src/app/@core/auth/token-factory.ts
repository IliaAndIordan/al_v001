import { Injectable, Inject } from '@angular/core';

@Injectable()
export class TokenFactory {

    public readonly __refreshTokenKey = 'refresh_token';

    // public headerName: string =  'Authorization';
    // public authScheme: string = 'Bearer ';
    // tslint:disable-next-line:no-inferrable-types
    public skipWhenExpired: boolean = false;
    public  throwNoTokenError = false;
    public whitelistedDomains: string[] = [
        // Your domains string[] or RegExp[]
        'ams.ws.ams.iordanov.info',
        'wad.ws.ams.iordanov.info',
        'common.ams.iord',
        // 'http://wad.ws.ams.iordanov.info/airports_edit/countrystate/',
        // 'http://ws.vlb.iordanov.info/vehicles',
    ];

    // constructor(@Inject(localStorage) protected localStorage: any) { }
    constructor() { }
    public tokenGetter = () => {
        const token = localStorage.getItem(this.__refreshTokenKey);
        // console.log('TokenFactory.tokenGetter token:' + token);
        return token;
    }
}
