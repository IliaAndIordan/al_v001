import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenFactory } from './token-factory';
import { HttpHeaders, HttpResponse, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { resolve } from 'q';

@Injectable()
export class TokenService extends TokenFactory {

  private baseUrl = 'https://ams.ws.ams.iordanov.info/';
  private refreshTokenUrl = this.baseUrl + 'jwtrefresh';

  constructor(private jwtHelper: JwtHelperService,
    private client: HttpClient) {
    super();
  }

  //#region JwtHelperService methods

  public isTokenExpired(): boolean {
    return this.jwtHelper.isTokenExpired();
  }

  public getTokenExpirationDate(): Date {
    return this.jwtHelper.getTokenExpirationDate();
  }

  public decodeToken(): string {
    return this.jwtHelper.decodeToken();
  }

  //#endregion

  //#region Token

  public set barerToken(value: string) {
    if (!value) {
      return;
    }

    localStorage.setItem(this.__refreshTokenKey, value);
  }

  public get barerToken(): string {
    return localStorage.getItem(this.__refreshTokenKey);
  }

  public clearToken() {
    localStorage.removeItem(this.__refreshTokenKey);
  }

  //#endregion
}
