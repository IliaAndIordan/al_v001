import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';


// Services
import { TokenService } from '../token.service';
import { CurrentUserService } from '../current-user.service';
import { UserModel, ResponseAuthenticate } from './dto';
import { ToastrService } from 'ngx-toastr';

// Models

// import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable()
export class AuthService {

  private baseUrl = 'https://ams.ws.ams.iordanov.info/';
  private loginUrl = this.baseUrl + 'login';
  private refreshTokenUrl = this.baseUrl + 'jwtrefresh';
  private registerUrl = this.baseUrl + 'register';

  private refreshTokenTimerId: any;

  constructor(
    private client: HttpClient,
    private tokenService: TokenService,
    private toastr: ToastrService,
    private currentUserService: CurrentUserService) { }

  register(email: string, password: string): Observable<any> {
    console.log('e-mail: ' + email + ', psw: ' + password);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.registerUrl,
      { email: email, password: password },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((responce: HttpResponse<any>) => this.convertAutenticateToUser(responce)),
        /*
        tap((res: HttpResponse<any>) => {
          console.log(res.headers.keys);
        }),*/
        catchError(this.handleError('autenticate', []))
      );
    /*
      .map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      });
      */
  }

  autenticate(username: string, password: string): Observable<any> {
    console.log('user: ' + username + ', ped: ' + password);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.loginUrl,
      { username: username, password: password },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((responce: HttpResponse<any>) => this.convertAutenticateToUser(responce)),
        /*
        tap((res: HttpResponse<any>) => {
          console.log(res.headers.keys);
        }),*/
        catchError(this.handleError('autenticate', []))
      );
    /*
      .map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      });
      */
  }

  refreshToken(): Observable<any> {

    console.log('AuthService:refreshToken: ->');
    console.log('AuthService:refreshToken: -> barerToken', this.tokenService.barerToken);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(this.refreshTokenUrl,
      { token: this.tokenService.barerToken },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((response: HttpResponse<any>) => this.convertAutenticateToUser(response)),
        /*
        tap((responce: HttpResponse<any>) => {
          console.dir(responce);
          this.toastr.info('Token Refreshed', 'System Information');
        }),*/
        catchError(this.handleError('refreshToken', []))
      );
  }

  convertAutenticateToUser(response: HttpResponse<any>): ResponseAuthenticate {
    let userObj;
    let airlineObj;
    let token;
    let rv;
    console.log('convertAutenticateToUser->response:', response);
    if (response) {
      if (response.headers) {
        token = response.headers.get('X-Authorization');
      }

      if (response.body) {
        rv = Object.assign(new ResponseAuthenticate(), response.body);
        console.log('convertAutenticateToUser ResponseAuthenticate:', rv);
        if (rv.data && rv.data.current_user) {
          userObj = rv.data.current_user; // Object.assign( new ResponseAuthenticate(), res.data.current_user) ;
          console.dir(userObj);
          console.log(' User: ' + userObj.name + ' auth.');
        } /* else {
          this.toastr.error(res.message, 'Autentication Failed.');
        }*/

        if (rv.data && rv.data.airline) {
          airlineObj = rv.data.airline; // Object.assign( new ResponseAuthenticate(), res.data.current_user) ;
          console.dir(airlineObj);
          console.log(' Airline: ' + airlineObj.id + ' .');
        }
      }


      if (token) {
        this.tokenService.barerToken = token;
        /*
        if (!this.refreshTokenTimerId) {
          this.refreshTokenTimerId = setInterval(function () {
            if (!this.tockenService.barerToken) {
              clearInterval(this.refreshTokenTimerId);
              this.refreshTokenTimerId = null;
              return;
            }
            console.log(' ---- begin setInterval ----');
            this.refreshToken()
            .subscribe(user => {
              if (user) {
                this.toastr.success('Welcome back ' + user.name + '!', 'Login success');
              } else {
                this.toastr.successr('Login Failed', 'Login Failed');
              }
            });
            console.log(' ---- end setInterval ----');
          }.bind(this), 5000);

        }
        */
        console.log(' ---- decode');
        console.log(this.tokenService.decodeToken());
        this.currentUserService.user = userObj;
        this.currentUserService.airline = airlineObj;
      }

    }

    // console.dir(retvalue);
    return rv;
  }

  logout() {
    // remove user from local storage to log user out
    console.log('AuthService::logout: ');
    this.tokenService.clearToken();
    this.currentUserService.user = undefined;
    this.currentUserService.airline = undefined;
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      console.log('operation: ' + operation);
      if (operation === 'refreshToken') {
        this.logout();
      } else {
        // TODO: better job of transforming error for user consumption
        this.toastr.error(`${operation} failed: ${error.message}`, 'Autentication');
      }


      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a  message with the toastr */
  private log(message: string) {
    this.toastr.info(message, 'Autentication');
  }
}
