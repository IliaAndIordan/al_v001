import { COMMON_IMAGE_URL } from '../../const/ams-storage-key.const';
import { DateModel, IDateModel } from '../../models/date.model';
import { LocalUserSettings, ILocalUserSettings } from '../local-user-settings';

export class AirlineModel {
    public airline_id: number;
    public name: string;
    public code: string;
    public logo: string;
    public slogan: string;
    public description: string;

    public user_id: number;
    public hq_airport_id: number;

    public logoUrl: string;
    public liveryUrl: string;

    public created: DateModel;
    public updated: DateModel;

    public static fromJSON(json: IAirlineModel): AirlineModel {
        const vs = Object.create(AirlineModel.prototype);
        return Object.assign(vs, json, {
            logoUrl: json.logoUrl ? json.logoUrl :
                COMMON_IMAGE_URL + 'airline/logo_' + json.airline_id + '.png',
            liveryUrl: json.liveryUrl ? json.liveryUrl :
                COMMON_IMAGE_URL + 'airline/livery_' + json.airline_id + '.png',
            created: (json && json.created) ? DateModel.fromJSON(json.created) : undefined,
            updated: (json && json.updated) ? DateModel.fromJSON(json.updated) : undefined
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? AirlineModel.fromJSON(value) : value;
    }

    public toJSON(): IAirlineModel {
        const vs = Object.create(AirlineModel.prototype);
        return Object.assign(vs, this);
    }
}

export interface IAirlineModel {
    airline_id: number;
    name: string;
    code: string;

    user_id: number;
    hq_airport_id: number;

    logo: string;
    slogan: string;
    description: string;

    logoUrl: string;
    liveryUrl: string;



    created: IDateModel;
    updated: IDateModel;
}

export class UserModel {

    public id: number;
    public eMail: string;
    public name: string;
    // 2 predefined roles 0-user, 1 admin, 2 editor
    public role = 0;
    public roleName: string;
    public isReceiveEMails: boolean;
    public ipAddress: string;
    public lastLogged: Date;
    public updated: Date;
}

export enum UserRole {
    User = 1,
    Editor = 2,
    Admin = 3
}
export class ResponseAuthenticateUser {
    public current_user: UserModel;
    public airline: AirlineModel;
    public settings: ILocalUserSettings;
}

export class ResponseAuthenticate {
    public status: string;
    public data: ResponseAuthenticateUser;
    public message: string;
}


