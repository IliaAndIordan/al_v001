import { NgModule, Optional, SkipSelf } from '@angular/core';
import { JwtModule, JWT_OPTIONS, JwtHelperService } from '@auth0/angular-jwt';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { GravatarModule } from '@infinitycube/gravatar';

import { throwIfAlreadyLoaded } from './guards/module-import.gard';

import { TokenService } from './auth/token.service';
import { TokenFactory } from './auth/token-factory';
import { ApiHelper } from './api/api.helper';
import { AuthService } from './auth/api/auth.service';
import { CurrentUserService } from './auth/current-user.service';
import { AirportEditService } from './api/airport-edit/airport-edit.service';

import { AuthGuard } from './guards/aut-guard.service';
import { AirportService } from './api/airport/airport.service';
import { AirlineService } from './api/airline/airline.service';
import { AirportRunwayService } from './api/airport-runway/airport-runway.service';
import { AirportDataService } from './api/airport/airport-data.service';
import { AircraftService } from './api/aircraft/aircraft.service';
import { AcMfrService } from './api/aircraft/ac-mfr.service';
import { AcMacService } from './api/aircraft/ac-mac.service';

@NgModule({
    imports: [
        HttpClientModule,
        /*GravatarModule,*/
        JwtModule.forRoot({
            jwtOptionsProvider: {
                provide: JWT_OPTIONS,
                useClass: TokenFactory
            }
        }),
        GravatarModule,
    ],
    exports: [
    ],
    providers: [
        ApiHelper,
        TokenService,
        AuthService,
        TokenFactory,
        CurrentUserService,
        AirportService,
        AirportDataService,
        AirportRunwayService,
        AirportEditService,
        AirlineService,
        AcMfrService,
        AcMacService,
        AircraftService,

        AuthGuard,
        /*SpinnerService,
        */
    ]
})

export class AmsCoreModule {
    constructor(@Optional() @SkipSelf() parentModule: AmsCoreModule) {
        throwIfAlreadyLoaded(parentModule, 'AmsCoreModule');
    }
}
