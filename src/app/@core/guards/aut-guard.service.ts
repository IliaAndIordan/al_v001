import { Injectable } from '@angular/core';
import { Router, Route, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, CanLoad } from '@angular/router';
import { AppRoutes } from '../const/app-routes.const';
import { CurrentUserService } from '../auth/current-user.service';
import { TokenService } from '../auth/token.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

  constructor(private router: Router,
    private currentUserService: CurrentUserService,
    private tokenService: TokenService) { }

  canLoad(route: Route) {
    return true;
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    if (this.currentUserService.isAuthenticated) {
      console.log('airline', this.currentUserService.airline);
      if (!this.currentUserService.airline) {
        if (state.url !== '/' + AppRoutes.Airline &&
          state.url !== '/' + AppRoutes.AirlineCreate) {
          this.router.navigate(['/', AppRoutes.AirlineCreate], { queryParams: { redirectTo: state.url } });
          return false;
        }
      }
      return true;
    }
    console.log('state.url=' + state.url);
    // Public rootes
    if (state.url === '/' + AppRoutes.Public ||
      state.url === '/' + AppRoutes.Manufacturers ||
      state.url === '/' + AppRoutes.Manufacturers + '/' + AppRoutes.Manufacturer ||
      state.url.startsWith('/' + AppRoutes.Manufacturers + '/' + AppRoutes.Mac)) {
      return true;
    }
    console.log('state.url=' + state.url);
    this.router.navigate(['/', AppRoutes.Public], { queryParams: { redirectTo: state.url } });
    return false;
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }
}
