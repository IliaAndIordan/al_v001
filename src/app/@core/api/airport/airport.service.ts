import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap, publishReplay, refCount } from 'rxjs/operators';
// Services
import { ServiceNames } from '../api.constants';
import { ApiHelper } from '../api.helper';
import { CurrentUserService } from '../../auth/current-user.service';
import { TokenService } from '../../auth/token.service';
import { ToastrService } from 'ngx-toastr';
import { ResponseAirportListModel, AirportModel } from './dto';

const KEY_SELECTED_COUNTRY_ID = 'KEY_SELECTED_COUNTRY_ID';
const KEY_SELECTED_STATE_ID = 'KEY_SELECTED_STATE_ID';
const KEY_SELECTED_AP_ID = 'KEY_SELECTED_AP_ID';


@Injectable()
export class AirportService {

    constructor(private apiHelper: ApiHelper,
        private http: HttpClient,
        private toastr: ToastrService,
        private currUserService: CurrentUserService,
        private tokenService: TokenService) { }

    //#region URL Methods

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.airport);
    }

    //#endregion


    //#region Airport lists

    private _airportListDbAllResponce: Observable<ResponseAirportListModel> = undefined;

    //#endregion

    //#region Country State Status Airport Count

    public getCountryListGvdt(): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/countrygvdt', null, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`getCountryListGvdt`); }),
                catchError(this.handleError('getCountryListGvdt', []))
            );
    }


    public getCountryList(): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/countrylist', null, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`getCountryList`); }),
                catchError(this.handleError('getCountryList', []))
            );
    }

    public getCountryRegionList(countryId: number): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/country_region_list',
            { country_id: countryId }, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`getCountryRegionList`); }),
                catchError(this.handleError('getCountryRegionList', []))
            );
    }


    public getAirportListDbAll(): Observable<any> {
        if (!this._airportListDbAllResponce) {
            const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
            this._airportListDbAllResponce =
                this.http.post(this.sertviceUrl + '/airports_db_all', null, { headers: hdrs }).pipe(
                    map((res: ResponseAirportListModel) => res),
                    tap(res => { this.log(`getAirportListDbAll`); }),
                    publishReplay(1),
                    refCount(),
                );
        }
        return this._airportListDbAllResponce;
    }

    clearCacheAirportListDbAll() {
        this._airportListDbAllResponce = undefined;
    }



    public getAirportListByState(stateId): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/ap_list_by_state',
            { state_id: stateId }, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`getAirportListByState`); }),
                catchError(this.handleError('getAirportListByState', []))
            );
    }

    public saveAirport(ap: AirportModel): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

        const options = { headers: hdrs, withCredentials: true };
        options.withCredentials = true;
        return this.http.post(this.sertviceUrl + '/ap_save',
            { airport: ap }, { headers: hdrs})
            .pipe(
                tap(res => { this.log(`saveAirport`); }),
                catchError(this.handleError('saveAirport', []))
            );
    }


    //#endregion

    //#region Base

    /**
  * Handle Http operation that failed.
  * Let the app continue.
  * @param operation - name of the operation that failed
  * @param result - optional value to return as the observable result
  */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            // this.toastr.error(`${operation} failed: ${error.message}`, 'WAD Service');

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a Service message  */
    private log(message: string) {
        // console.log('WAD Service', message);
        // this.toastr.info(message, 'WAD Service');
    }

    //#endregion
}
