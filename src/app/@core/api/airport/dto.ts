import { FLAGS_IMAGE_URL } from '../../const/ams-storage-key.const';
import { NumberValueAccessor } from '@angular/forms/src/directives';
import { PieMiniChartViewModel } from '../../models/pie-chart.model';

export class AirportModel {
    active: boolean;
    apId: number;
    apName: string;
    apStatus: string;
    apStatusId: number;
    apType: string;
    apTypeBase: number;
    apTypeId: number;
    cityId: number;
    cityName: string;
    cityPopulation: number;
    cityWikiUrl: string;
    countryId: number;
    countryIso: string;
    countryName: string;
    region: string;
    sunRegion: string;
    countryIcao: string;
    countryWikiLink: string;
    homeUrl: string;
    iata: string;
    icao: string;
    lat: number;
    lon: number;
    notes: string;
    stateId: number;
    stateName: string;
    utc: number;
    wikiUrl: string;
    apSvgCircle: string;
    apSvgRwLines: string;
    rwLenghtMaxM: number;
    rwMtowMaxKg: number;

    countryFlagUrl: string;

    public static fromJSON(json: IAirportModel): AirportModel {
        const vs = Object.create(AirportModel.prototype);
        return Object.assign(vs, json, {
            apName: json.apName,
            active: json.active > 0 ? true : false,
            apType: json.apType,
            countryFlagUrl: FLAGS_IMAGE_URL + json.countryIso.toLowerCase() + '.png',
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? CountryStateModel.fromJSON(value) : value;
    }

    public toJSON(): IAirportModel {
        return Object.assign({}, this, {
            apName: this.apName ? this.apName : '',
            active: this.active ? 1 : 0,
            apType: this.apType,
        });
    }
}

export interface IAirportModel {
    active: number;
    apId: number;
    apName: string;
    apStatus: string;
    apStatusId: number;
    apType: string;
    apTypeBase: number;
    apTypeId: number;
    cityId: number;
    cityName: string;
    cityPopulation: number;
    cityWikiUrl: string;
    countryId: number;
    countryIso: string;
    countryName: string;
    region: string;
    sunRegion: string;
    countryIcao: string;
    countryWikiLink: string;
    homeUrl: string;
    iata: string;
    icao: string;
    lat: number;
    lon: number;
    notes: string;
    stateId: number;
    stateName: string;
    utc: number;
    wikiUrl: string;
    apSvgCircle: string;
    apSvgRwLines: string;
    rwLenghtMaxM: number;
    rwMtowMaxKg: number;
}


export class IAirportListModel {
    airport_list: Array<IAirportModel>;

    getAirportModelList(): Array<AirportModel> {
        const rv = new Array<AirportModel>();
        if (this.airport_list) {
            this.airport_list.forEach((element: IAirportModel) => {


                const tmp: AirportModel = AirportModel.fromJSON(element);
                rv.push(tmp);
            });
        }

        return rv;
    }
}

export interface ResponseAirportListModel {
    status: string;
    message: string;
    data: IAirportListModel;
}

export class CountryStateModel {
    airports: number;
    apActive: number;
    iso: string;
    name: string;
    stateId: number;
    countryId: number;
    wikiLinkUrl: string;

    apActivePct: number;
    pieChart: PieMiniChartViewModel;

    public static fromJSON(json: ICountryStateModel): CountryStateModel {
        const vs = Object.create(CountryStateModel.prototype);
        return Object.assign(vs, json, {
            name: json.sname,
            apActivePct: (json.apActive / json.airports),
            pieChart: PieMiniChartViewModel.fromCountryStateModel(json)
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? CountryStateModel.fromJSON(value) : value;
    }

    public toJSON(): ICountryStateModel {
        return Object.assign({}, this, {
            sname: this.name ? this.name : '',
        });
    }
}

export interface ICountryStateModel {
    airports: number;
    apActive: number;
    iso: string;
    sname: string;
    stateId: number;
    countryId: number;
    wikiLinkUrl: string;
}

export class ICountryStateListModel {
    countyStateList: Array<ICountryStateModel>;

    getCountryModelList(): Array<CountryStateModel> {
        const rv = new Array<CountryStateModel>();
        if (this.countyStateList) {
            this.countyStateList.forEach((element: ICountryStateModel) => {


                const tmp: CountryStateModel = CountryStateModel.fromJSON(element);
                rv.push(tmp);
            });
        }

        return rv;
    }
}

export interface ResponseCountryRegionListModel {
    status: string;
    message: string;
    data: ICountryStateListModel;
}

//#region Airport List by Country State



export class CountryModel {

    id: number;
    airports: number;
    apActive: number;
    iso2: string;
    name: string;
    region: string;
    subRegion: string;
    wikilink: string;
    icao: string;
    flagUrl: string;
    apActivePct: number;

    public static fromJSON(json: ICountryModel): CountryModel {
        const vs = Object.create(CountryModel.prototype);
        return Object.assign(vs, json, {
            name: json.cname,
            flagUrl: FLAGS_IMAGE_URL + json.iso2.toLowerCase() + '.png',
            apActivePct: (json.apActive / json.airports)
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? CountryModel.fromJSON(value) : value;
    }

    public toJSON(): ICountryModel {
        return Object.assign({}, this, {
            cname: this.name ? this.name : '',
        });
    }
}

export interface ICountryModel {

    id: number;
    airports: number;
    apActive: number;
    iso2: string;
    cname: string;
    region: string;
    subRegion: string;
    wikilink: string;
    icao: string;

}

export class ICountryListModel {
    countyList: Array<ICountryModel>;

    getCountryModelList(): Array<CountryModel> {
        const rv = new Array<CountryModel>();
        if (this.countyList) {
            this.countyList.forEach((element: ICountryModel) => {


                const tmp: CountryModel = CountryModel.fromJSON(element);
                rv.push(tmp);
            });
        }

        return rv;
    }
}

export interface ResponseCountryListModel {
    status: string;
    message: string;
    data: ICountryListModel;
}


export interface IGvdtModel {
    gvdt: Object;
}

export interface ResponseGvdtModel {
    status: string;
    message: string;
    data: IGvdtModel;
}
//#endregion

