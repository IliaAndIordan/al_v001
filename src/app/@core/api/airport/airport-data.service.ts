import { Injectable } from '@angular/core';
import { ApiHelper } from '../api.helper';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/observable';
import { tap, catchError } from 'rxjs/operators';
// Services
import { CurrentUserService } from '../../auth/current-user.service';
import { TokenService } from '../../auth/token.service';
// Constants
import { ServiceNames } from '../api.constants';
import { AmsStorageKeysLocal } from '../../const/ams-storage-key.const';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';
import { AirportModel, IAirportModel, ResponseAirportListModel } from './dto';

@Injectable()
export class AirportDataService {

    protected observable: Observable<any>;


    constructor(private apiHelper: ApiHelper,
        private http: HttpClient,
        private toastr: ToastrService,
        private currUserService: CurrentUserService,
        private tokenService: TokenService) { }

    //#region Data Cache

    private get airports(): Array<AirportModel> {
        const airports = new Array<AirportModel>();
        const data = localStorage.getItem(AmsStorageKeysLocal.AirportInfoChahe);
        if (data) {
            const parsedData: Array<IAirportModel> = JSON.parse(data);
            parsedData.forEach((element: IAirportModel) => {
                const tmp: AirportModel = AirportModel.fromJSON(element);
                airports.push(tmp);
            });
        }
        return airports;
    }

    public resetAirportsCache(): void {
        localStorage.removeItem(AmsStorageKeysLocal.AirportInfoChahe);
    }

    private updateAirportCashe(data: Array<AirportModel>): void {

        if (data) {

            // console.log('updateRwCashe-> data.length=', data.length);

            const rw = this.airports;
            // console.log('updateRwCashe-> data', data);

            data.forEach((element: AirportModel) => {

                // console.log('updateRwCashe-> element', element);

                const toUpdate = rw.find(x => x.apId === element.apId);
                // console.log('updateRwCashe-> toUpdate', toUpdate);
                if (toUpdate) {
                    const idx = rw.indexOf(toUpdate);
                    rw[idx] = element;
                } else {
                    rw.push(element);
                }
            });
            // console.log('updateRwCashe-> runways', rw);
            localStorage.setItem(AmsStorageKeysLocal.RunwayList, JSON.stringify(rw));

        }
    }

    private airportByIdFromCache(apId: number): Array<AirportModel> {
        let data = new Array<AirportModel>();
        if (apId) {
            const aps = this.airports;
            if (aps && aps.length > 0) {
                data = aps.filter(x => x.apId === apId);
            }
        }

        return data;
    }

    private airportByStateFromCache(stateId: number): Array<AirportModel> {
        let data = new Array<AirportModel>();
        if (stateId) {
            const aps = this.airports;
            if (aps && aps.length > 0) {
                data = aps.filter(x => x.stateId === stateId);
            }
        }

        return data;
    }

    private airportByCountryFromCache(countryId: number): Array<AirportModel> {
        let data = new Array<AirportModel>();
        if (countryId) {
            const aps = this.airports;
            if (aps && aps.length > 0) {
                data = aps.filter(x => x.countryId === countryId);
            }
        }

        return data;
    }
    //#endregion

    //#region Airports

    public airportByState(stateId: number): Observable<Array<AirportModel>> {

        let data = new Array<AirportModel>();
        if (stateId) {
            data = this.airportByStateFromCache(stateId);
            if (!data || data.length === 0) {

                this.observable = this.getAirportsByState(stateId)
                    .map((responce: ResponseAirportListModel) => {
                        this.observable = null;
                        data = new Array<AirportModel>();
                        if (responce && responce.data) {
                            responce.data.airport_list.forEach((element: IAirportModel) => {
                                const tmp: AirportModel = AirportModel.fromJSON(element);
                                data.push(tmp);
                            });
                            this.updateAirportCashe(data);
                        }

                        return data;
                    })
                    .share();

            } else {
                // console.log('data already available');
                return Observable.of(data);
            }
        }

        return this.observable;
    }

    public airportByCountry(countryId: number): Observable<Array<AirportModel>> {

        let data = new Array<AirportModel>();
        if (countryId) {
            data = this.airportByCountryFromCache(countryId);
            if (!data || data.length === 0) {

                this.observable = this.getAirportsByCountry(countryId)
                    .map((responce: ResponseAirportListModel) => {
                        this.observable = undefined;
                        data = new Array<AirportModel>();
                        if (responce && responce.data) {
                            responce.data.airport_list.forEach((element: IAirportModel) => {
                                const tmp: AirportModel = AirportModel.fromJSON(element);
                                data.push(tmp);
                            });
                            this.updateAirportCashe(data);
                        }

                        return data;
                    })
                    .share();

            } else {
                // console.log('data already available');
                return Observable.of(data);
            }
        }

        return this.observable;
    }

    public airportById(apId: number): Observable<AirportModel> {

        let data = new Array<AirportModel>();
        if (apId) {
            data = this.airportByIdFromCache(apId);
            if (!data || data.length === 0) {

                this.observable = this.getAirport(apId)
                    .map((responce: ResponseAirportListModel) => {
                        this.observable = undefined;
                        data = new Array<AirportModel>();
                        if (responce && responce.data) {
                            responce.data.airport_list.forEach((element: IAirportModel) => {
                                const tmp: AirportModel = AirportModel.fromJSON(element);
                                data.push(tmp);
                            });
                            this.updateAirportCashe(data);
                        }

                        return data[0];
                    })
                    .share();

            } else {
                // console.log('data already available');
                return Observable.of(data[0]);
            }
        }

        return this.observable;
    }

    //#endregion

    //#region Runway Http Calls

    private getAirportsByCountry(countryId): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/ap_list_by_country',
            { country_id: countryId }, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`getAirportsByCountry`); }),
                catchError(this.handleError('getAirportsByCountry', []))
            );
    }


    private getAirportsByState(stateId): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/ap_list_by_state',
            { state_id: stateId }, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`getAirportListByState`); }),
                catchError(this.handleError('getAirportListByState', []))
            );
    }

    private getAirport(airportId): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/airport',
            { airport_id: airportId }, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`getAirport`); }),
                catchError(this.handleError('getAirport', []))
            );
    }

    //#endregion

    //#region URL Methods

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.airport);
    }

    //#endregion

    //#region Base

    /**
  * Handle Http operation that failed.
  * Let the app continue.
  * @param operation - name of the operation that failed
  * @param result - optional value to return as the observable result
  */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            // this.toastr.error(`${operation} failed: ${error.message}`, 'WAD Service');

            // Let the app keep running by returning an empty result.
            return Observable.of(result as T);
        };
    }

    /** Log a Service message  */
    private log(message: string) {
        // console.log('Airport Runway Service', message);
        // this.toastr.info(message, 'WAD Service');
    }

    //#endregion
}
