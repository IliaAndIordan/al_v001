import { AirportViewModel } from '../../models/airpport.view-model';
import { IAirlineModel } from '../../auth/api/dto';
import { ResponseModel } from '../responce.model';


//#region  AlNameCodeModel

export class ResponseDataAirlineModel {
    airline: IAirlineModel;
}
export interface ResponseAirlineModel extends ResponseModel {
    data: ResponseDataAirlineModel;
}

export interface AirlineCreateData {
    user_id: number;
    al_name: string;
    al_code: string;
    hq_airport: AirportViewModel;
}

export class AlNameCodeModel {
    al_name: string;
    al_code: string;
}

export class AlNameCodeListAll {
    al_list_for_check: Array<AlNameCodeModel>;
}

export interface ResponseAlNameCodeListModel extends ResponseModel {
    data: AlNameCodeListAll;
}

//#endregion
