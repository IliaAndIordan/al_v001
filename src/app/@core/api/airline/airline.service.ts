import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap, publishReplay, refCount } from 'rxjs/operators';
// Services
import { ServiceNames } from '../api.constants';
import { ApiHelper } from '../api.helper';
import { CurrentUserService } from '../../auth/current-user.service';
import { TokenService } from '../../auth/token.service';
import { ToastrService } from 'ngx-toastr';
import { SUCCESS } from '../../const/global.const';
import { ResponseAlNameCodeListModel, AirlineCreateData, ResponseAirlineModel } from './dto';
import { IAirportViewModel } from '../../models/airpport.view-model';
import { IAirlineModel, AirlineModel } from '../../auth/api/dto';

const KEY_SELECTED_COUNTRY_ID = 'KEY_SELECTED_COUNTRY_ID';
const KEY_SELECTED_STATE_ID = 'KEY_SELECTED_STATE_ID';
const KEY_SELECTED_AP_ID = 'KEY_SELECTED_AP_ID';


@Injectable()
export class AirlineService {

    private _alListForCheckResponce: Observable<ResponseAlNameCodeListModel> = null;

    constructor(private apiHelper: ApiHelper,
        private http: HttpClient,
        private toastr: ToastrService,
        private currUserService: CurrentUserService,
        private tokenService: TokenService) { }

    //#region URL Methods

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.airline);
    }

    //#endregion

    //#region Registration


    public getAlListForCheckl(): Observable<ResponseAlNameCodeListModel> {
        if (!this._alListForCheckResponce) {
            const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
            this._alListForCheckResponce =
                this.http.post(this.sertviceUrl + '/al_list_for_check', null, { headers: hdrs }).pipe(
                    map((res: ResponseAlNameCodeListModel) => res),
                    tap(res => { this.log(`getAlListForCheckl`); }),
                    publishReplay(1),
                    refCount(),
                );
        }
        return this._alListForCheckResponce;
    }

    public alCreate(al: AirlineCreateData): Observable<ResponseAirlineModel> {
        al.user_id = this.currUserService.user.id;
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/al_create',
            { user_id: al.user_id, al_name: al.al_name, al_code: al.al_code, hq_airport_id: al.hq_airport.airport_id },
            { headers: hdrs }).pipe(
                map((res: ResponseAirlineModel) => res),
                tap(res => { this.log(`alCreate`); }),
            );
    }

    public getAirline(id: number): Observable<ResponseAirlineModel> {
        console.log('getAirline: airline_id=' + id);
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/airline',
            { 'airline_id': id },
            { headers: hdrs }).pipe(
                map((res: ResponseAirlineModel) => res),
                tap(res => { this.log(`getAirline`); }),
            );
    }

    public alSave(al: AirlineModel): Observable<ResponseAirlineModel> {
        if (al.user_id === this.currUserService.user.id ||
            this.currUserService.user.role === 1) {

            al.user_id = this.currUserService.user.id;
            const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
            return this.http.post(this.sertviceUrl + '/al_save',
                {airline: al},
                { headers: hdrs, withCredentials: true }).pipe(
                    map((res: ResponseAirlineModel) => res),
                    tap(res => { this.log(`alSave after`); }),
                );
        }
    }

    clearCacheAirportListDbAll() {
        this._alListForCheckResponce = undefined;
    }

    //#endregion

    //#region Base

    /**
  * Handle Http operation that failed.
  * Let the app continue.
  * @param operation - name of the operation that failed
  * @param result - optional value to return as the observable result
  */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            // this.toastr.error(`${operation} failed: ${error.message}`, 'WAD Service');

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a Service message  */
    private log(message: string) {
        console.log('Service', message);
        // this.toastr.info(message, 'WAD Service');
    }

    //#endregion
}
