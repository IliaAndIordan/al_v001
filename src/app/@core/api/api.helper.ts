import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
// Constants
import { ServiceNames, ServiceUrl } from './api.constants';
// Service
import { TokenService } from '../auth/token.service';
import { AuthService } from '../auth/api/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { ResponseAuthenticate } from '../auth/api/dto';


@Injectable()
export class ApiHelper {

    constructor(private tokenService: TokenService,
        private authService: AuthService,
        private toastr: ToastrService) { }

    public ServiceNames = ServiceNames;
    public ServiceUrl = ServiceUrl;

    protected refreshTokenOb: Observable<boolean>;
    protected getServiceUrlOb: Observable<string>;
    /**
     * @method getServiceUrl
     * @param serviceName ddd
     * @returns Service URL value as {string}
     */
    public getServiceUrl(serviceName: string): string {


        let retValue;
        this.refreshToken()
            .subscribe((res: boolean) => {
                console.log('ApiHelper::getServiceUrl-> res=', res);
                console.log('ApiHelper::getServiceUrl-> barerToken=', this.tokenService.barerToken);
                console.log('ApiHelper::getServiceUrl-> isTokenExpired=', this.tokenService.isTokenExpired());
            });
        const sn = ServiceNames[serviceName];
        // console.log('sn: ' + sn + ' for serviceName:' + serviceName);
        const urlFilter = ServiceUrl.filter(x => x.name === serviceName);
        // console.log('urlFilter.length:' + urlFilter.length);
        if (urlFilter && urlFilter.length > 0) {
            retValue = urlFilter[0].url;
        }
        console.log('sn: ' + sn + ' for serviceUrl:' + retValue);

        return retValue;

    }

    private refreshToken(): Observable<boolean> {
        const retVal = (this.tokenService.barerToken && this.tokenService.isTokenExpired());
        if (retVal) {
            if (this.tokenService.barerToken) {
                console.log('ApiHelper::refreshToken-> try to refresh token...');
                this.refreshTokenOb = this.authService.refreshToken()
                    .map((responce: ResponseAuthenticate) => {
                        console.log('ApiHelper::refreshToken-> barerToken=', this.tokenService.barerToken);
                        this.refreshTokenOb = Observable.of((this.tokenService.barerToken && this.tokenService.isTokenExpired()));
                        return (this.tokenService.barerToken && this.tokenService.isTokenExpired());
                    })
                    .share();
            } else {
                this.authService.logout();
                return Observable.of(retVal);
            }
        } else {
            return Observable.of(retVal);
        }

        return this.refreshTokenOb;
    }


}
