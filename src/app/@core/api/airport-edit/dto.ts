
//#region Country State Status Airport Count

export class CountryStateStatusAirportModel {
    public countryIso2: string;
    public countryName: string;
    public stateName: string;

    public airportsCount: number;
    public airportsCountAmsStatus0: number;
    public checked: boolean;
    public countryId: number;
    public stateId: number;

    public static fromJSON(json: ICountryStateStatusAirportModel): CountryStateStatusAirportModel {
        const vs = Object.create(CountryStateStatusAirportModel.prototype);
        return Object.assign(vs, json, {
            checked: json.checked ? (json.checked > 0 ? true : false) : false,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? CountryStateStatusAirportModel.fromJSON(value) : value;
    }

    public toJSON(): ICountryStateStatusAirportModel {
        return Object.assign({}, this, {
            checked: this.checked ? 1 : 0,
        });
    }
}

export interface ICountryStateStatusAirportModel {
    countryIso2: string;
    countryName: string;
    stateName: string;

    airportsCount: number;
    airportsCountAmsStatus0: number;
    checked: number;
    countryId: number;
    stateId: number;
}


//#endregion

//#region Responce / Request

export interface ResponseCountryStateStatusAirport {
    status: string;
    message: string;
    data: ICountryStateStatusAirportModel[];
}

//#endregion

//#region Airport List by Country State

export class AirportCountryStateModel {
    airport_id: number;
    ams_status: number;
    ams_status_name: string;
    ap_name: string;
    ap_type_name: string;
    atype_base: number;
    city_name: string;

    public static fromJSON(json: IAirportCountryStateModel): AirportCountryStateModel {
        const vs = Object.create(AirportCountryStateModel.prototype);
        return Object.assign(vs, json);
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? CountryStateStatusAirportModel.fromJSON(value) : value;
    }

    public toJSON(): IAirportCountryStateModel {
        return Object.assign({}, this);
    }
}

export interface IAirportCountryStateModel {
    airport_id: number;
    ams_status: number;
    ams_status_name: string;
    ap_name: string;
    ap_type_name: string;
    atype_base: number;
    city_name: string;
}

export interface ResponseAirportCountryStateModel {
    status: string;
    message: string;
    data: IAirportCountryStateModel[];
}
//#endregion

