import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';
import { Observable, Subject } from 'rxjs';
// Custom
import { ApiHelper } from '../api.helper';
import { CurrentUserService } from '../../auth/current-user.service';
import { ServiceNames } from '../api.constants';
import { AmsStorageKeysLocal } from '../../const/ams-storage-key.const';
import { BaseService } from '../api.service';
import { tap, catchError } from 'rxjs/operators';
import { AcMfrModel, IAcMfrModel, RespAcMfrList } from './dto';


@Injectable()
export class AcMfrService extends BaseService {

    protected observable: Observable<any>;
    currMfrChanged = new Subject<AcMfrModel>();

    constructor(private apiHelper: ApiHelper,
        private http: HttpClient,
        private currUserService: CurrentUserService) {
        super();
    }

    //#region Data

    get currMfr(): AcMfrModel {
        let rv: AcMfrModel;
        const data = localStorage.getItem(AmsStorageKeysLocal.CurrMfr);
        if (data) {
            const parsedData: IAcMfrModel = JSON.parse(data);
            if (parsedData) {
                rv = AcMfrModel.fromJSON(parsedData);
            }
        }
        return rv;
    }

    set currMfr(value: AcMfrModel) {
        if (value) {
            localStorage.setItem(AmsStorageKeysLocal.CurrMfr, JSON.stringify(value));
        } else {
            localStorage.removeItem(AmsStorageKeysLocal.CurrMfr);
        }
        this.currMfrChanged.next(value);
    }

    private get manufacturers(): Array<AcMfrModel> {
        const runways = new Array<AcMfrModel>();
        const data = localStorage.getItem(AmsStorageKeysLocal.AcMfrList);
        if (data) {
            const parsedData: Array<IAcMfrModel> = JSON.parse(data);
            parsedData.forEach((element: IAcMfrModel) => {
                const tmp: AcMfrModel = AcMfrModel.fromJSON(element);
                runways.push(tmp);
            });
        }
        return runways;
    }

    public resetAcMfrCache(): void {
        localStorage.removeItem(AmsStorageKeysLocal.AcMfrList);
    }

    private updateAcMfrCashe(data: Array<AcMfrModel>): void {

        if (data) {

            // console.log('updateRwCashe-> data.length=', data.length);

            const rw = this.manufacturers;
            // console.log('updateRwCashe-> data', data);

            data.forEach((element: AcMfrModel) => {

                // console.log('updateRwCashe-> element', element);

                const toUpdate = rw.find(x => x.mfr_id === element.mfr_id);
                console.log('updateAcMfrCashe-> toUpdate', toUpdate);
                if (toUpdate) {
                    const idx = rw.indexOf(toUpdate);
                    rw[idx] = element;
                } else {
                    rw.push(element);
                }
            });
            // console.log('updateRwCashe-> runways', rw);
            localStorage.setItem(AmsStorageKeysLocal.AcMfrList, JSON.stringify(rw));

        }
    }

    private acMfrFromCache(id: number): Array<AcMfrModel> {
        let data = new Array<AcMfrModel>();
        if (id) {
            const cacheData = this.manufacturers;
            if (cacheData && cacheData.length > 0) {
                data = cacheData.filter(x => x.mfr_id === id);
            }
        }

        return data;
    }

    public acMfr(id: number): Observable<Array<AcMfrModel>> {

        let data = new Array<AcMfrModel>();
        if (id) {
            data = this.acMfrFromCache(id);
            if (!data || data.length === 0) {

                this.observable = this.getAcMfrList()
                    .map((responce: RespAcMfrList) => {
                        this.observable = undefined;
                        data = new Array<AcMfrModel>();
                        if (responce.data.ac_mfr_list) {
                            responce.data.ac_mfr_list.forEach((element: IAcMfrModel) => {
                                const tmp: AcMfrModel = AcMfrModel.fromJSON(element);
                                data.push(tmp);
                            });
                            this.updateAcMfrCashe(data);
                            data = this.acMfrFromCache(id);
                        }

                        return data;
                    })
                    .share();

            } else {
                // console.log('data already available');
                return Observable.of(data);
            }
        }

        return this.observable;
    }

    public acMfrList(): Observable<Array<AcMfrModel>> {

        let data = this.manufacturers;
        if (!data || data.length === 0) {

            this.observable = this.getAcMfrList()
                .map((responce: RespAcMfrList) => {
                    this.observable = undefined;
                    data = new Array<AcMfrModel>();
                    if (responce.data.ac_mfr_list) {
                        responce.data.ac_mfr_list.forEach((element: IAcMfrModel) => {
                            const tmp: AcMfrModel = AcMfrModel.fromJSON(element);
                            data.push(tmp);
                        });
                        this.updateAcMfrCashe(data);
                    }

                    return data;
                })
                .share();

        } else {
            // console.log('data already available');
            return Observable.of(data);
        }

        return this.observable;
    }

    //#endregion

    //#region Runway Http Calls

    public getAcMfrList(): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/ac_mfr_list',
            null, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`getRunwayListByairport`); }),
                catchError(this.handleError('getRunwayListByairport', []))
            );
    }

    //#endregion

    //#region URL Methods

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.aircraft);
    }

    //#endregion
}
