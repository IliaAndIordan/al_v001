import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// Custom
import { ApiHelper } from '../api.helper';
import { CurrentUserService } from '../../auth/current-user.service';
import { ServiceNames } from '../api.constants';

@Injectable()
export class AircraftService {

    constructor(private apiHelper: ApiHelper,
        private http: HttpClient,
        private currUserService: CurrentUserService) { }

    //#region URL Methods

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.aircraft);
    }

    //#endregion

}
