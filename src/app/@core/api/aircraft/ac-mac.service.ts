import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
// Custom
import { ApiHelper } from '../api.helper';
import { CurrentUserService } from '../../auth/current-user.service';
import { ServiceNames } from '../api.constants';
import { AmsStorageKeysLocal } from '../../const/ams-storage-key.const';
import { BaseService } from '../api.service';
import { tap, catchError } from 'rxjs/operators';
import { AcMacModel, IAcMacModel, RespAcMacList } from './dto';

@Injectable()
export class AcMacService extends BaseService {

    protected observable: Observable<any>;

    constructor(private apiHelper: ApiHelper,
        private http: HttpClient,
        private currUserService: CurrentUserService) {
        super();
    }

    //#region Data

    private get mac(): Array<AcMacModel> {
        const runways = new Array<AcMacModel>();
        const data = localStorage.getItem(AmsStorageKeysLocal.AcMacList);
        if (data) {
            const parsedData: Array<IAcMacModel> = JSON.parse(data);
            parsedData.forEach((element: IAcMacModel) => {
                const tmp: AcMacModel = AcMacModel.fromJSON(element);
                runways.push(tmp);
            });
        }
        return runways;
    }

    public resetAcMacCache(): void {
        localStorage.removeItem(AmsStorageKeysLocal.AcMacList);
    }

    private updateAcMacCashe(data: Array<AcMacModel>): void {

        if (data) {

            // console.log('updateRwCashe-> data.length=', data.length);

            const cacheData = this.mac;
            // console.log('updateRwCashe-> data', data);

            data.forEach((element: AcMacModel) => {

                // console.log('updateRwCashe-> element', element);

                const toUpdate = cacheData.find(x => x.mac_id === element.mac_id);
                // console.log('updateRwCashe-> toUpdate', toUpdate);
                if (toUpdate) {
                    const idx = cacheData.indexOf(toUpdate);
                    cacheData[idx] = element;
                } else {
                    cacheData.push(element);
                }
            });
            // console.log('updateRwCashe-> runways', rw);
            localStorage.setItem(AmsStorageKeysLocal.AcMacList, JSON.stringify(cacheData));

        }
    }

    private acMacFromCache(id: number): Array<AcMacModel> {
        let data = new Array<AcMacModel>();
        if (id) {
            const cacheData = this.mac;
            if (cacheData && cacheData.length > 0) {
                console.log('acMacFromCach cacheData', cacheData);
                data = cacheData.filter(x => x.mac_id === id);
            }
        }
        console.log('acMacFromCache', data);
        return data;
    }

    private acMacByMfrFromCache(mfr_id: number): Array<AcMacModel> {
        let data = new Array<AcMacModel>();
        if (mfr_id) {
            const cacheData = this.mac;
            if (cacheData && cacheData.length > 0) {
                data = cacheData.filter(x => x.mfr_id === mfr_id);
            }
        }

        return data;
    }

    public acMac(id: number): Observable<Array<AcMacModel>> {

        let data = new Array<AcMacModel>();
        if (id) {
            data = this.acMacFromCache(id);
            if (!data || data.length === 0) {

                this.observable = this.getAcMacList()
                    .map((responce: RespAcMacList) => {
                        this.observable = undefined;
                        data = new Array<AcMacModel>();
                        if (responce.data.mac_list) {
                            responce.data.mac_list.forEach((element: IAcMacModel) => {
                                const tmp: AcMacModel = AcMacModel.fromJSON(element);
                                data.push(tmp);
                            });
                            this.updateAcMacCashe(data);
                            data = this.acMacFromCache(id);
                        }

                        return data;
                    })
                    .share();

            } else {
                // console.log('data already available');
                return Observable.of(data);
            }
        }

        return this.observable;
    }

    public acMacByMfr(mfr_id: number): Observable<Array<AcMacModel>> {

        let data = new Array<AcMacModel>();
        if (mfr_id) {
            data = this.acMacByMfrFromCache(mfr_id);
            if (!data || data.length === 0) {

                this.observable = this.getAcMacList()
                    .map((responce: RespAcMacList) => {
                        this.observable = undefined;
                        data = new Array<AcMacModel>();
                        if (responce.data.mac_list) {
                            responce.data.mac_list.forEach((element: IAcMacModel) => {
                                const tmp: AcMacModel = AcMacModel.fromJSON(element);
                                data.push(tmp);
                            });
                            this.updateAcMacCashe(data);
                            data = this.acMacByMfrFromCache(mfr_id);
                        }

                        return data;
                    })
                    .share();

            } else {
                // console.log('data already available');
                return Observable.of(data);
            }
        }

        return this.observable;
    }

    public acMacList(): Observable<Array<AcMacModel>> {

        let data = this.mac;
        if (!data || data.length === 0) {

            this.observable = this.getAcMacList()
                .map((responce: RespAcMacList) => {
                    this.observable = undefined;
                    data = new Array<AcMacModel>();
                    if (responce.data.mac_list) {
                        responce.data.mac_list.forEach((element: IAcMacModel) => {
                            const tmp: AcMacModel = AcMacModel.fromJSON(element);
                            data.push(tmp);
                        });
                        this.updateAcMacCashe(data);
                    }

                    return data;
                })
                .share();

        } else {
            // console.log('data already available');
            return Observable.of(data);
        }

        return this.observable;
    }

    //#endregion

    //#region Runway Http Calls

    public getAcMacList(): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/mac_list',
            null, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`getAcMacList`); }),
                catchError(this.handleError('getAcMacList', []))
            );
    }

    //#endregion

    //#region URL Methods

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.aircraft);
    }

    //#endregion
}
