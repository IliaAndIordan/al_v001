import { ResponseModel } from '../responce.model';
import { FLAGS_IMAGE_URL, AC_IMAGE_URL } from '../../const/ams-storage-key.const';

//#region  AcMfrModel

export class AcMfrModel {

    mfr_headquartier: string;
    mfr_iata: string;
    mfr_id: number;
    mfr_logo: string;
    mfr_name: string;
    mfr_notes: string;
    mfr_web: string;
    mfr_wikilink: string;

    airport_id: number;
    ap_iata: string;
    ap_icao: string;
    ap_lat: number;
    ap_lon: number;
    ap_name: string;
    city_id: number;
    city_name: string;
    city_wiki_url: string;
    country_id: number;
    country_iso2: string;
    country_name: string;
    region: string;
    state_id: number;
    state_name: string;
    sub_region: string;

    logoUrl: string;
    countryFlagUrl: string;

    public static fromJSON(json: IAcMfrModel): AcMfrModel {
        const vs = Object.create(AcMfrModel.prototype);
        return Object.assign(vs, json, {
            countryFlagUrl: FLAGS_IMAGE_URL + json.country_iso2.toLowerCase() + '.png',
            logoUrl: AC_IMAGE_URL + json.mfr_logo,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? AcMfrModel.fromJSON(value) : value;
    }

    public toJSON(): IAcMfrModel {
        return Object.assign({}, this);
    }
}

export interface IAcMfrModel {
    airport_id: number;
    ap_iata: string;
    ap_icao: string;
    ap_lat: number;
    ap_lon: number;
    ap_name: string;
    city_id: number;
    city_name: string;
    city_wiki_url: string;
    country_id: number;
    country_iso2: string;
    country_name: string;
    mfr_headquartier: string;
    mfr_iata: string;
    mfr_id: number;
    mfr_logo: string;
    mfr_name: string;
    mfr_notes: string;
    mfr_web: string;
    mfr_wikilink: string;
    region: string;
    state_id: number;
    state_name: string;
    sub_region: string;
}

export class AcMfrListModel {
    ac_mfr_list: Array<IAcMfrModel>;
}

export interface RespAcMfrList extends ResponseModel {
    data: AcMfrListModel;
}

//#endregion

//#region AcMacModel

export class AcMacModel {

    mac_id: number;
    mfr_id: number;
    model: string;
    ac_type_id: number;
    max_range_km: number;
    fuel_tank_l: number;
    pilots: number;
    crew_seats: number;
    max_seating: number;
    useful_load_kg: number;
    price: number;
    avg_fuel_consumption_lp100km: number;
    max_cruise_altitude_m: number;
    cruise_speed_kmph: number;
    take_off_m: number;
    landing_m: number;
    operation_time: string;
    cost_per_fh: number;
    asmi_rate: number;
    wiki_link: string;
    powerplant: string;
    production_start: string;
    production_rate_h: number;
    last_produced_on: string;
    notes: string;
    popularity: number;
    number_build: number;
    mtwo_kg: number;
    mtow_empty: number;
    seats_per_row: number;
    cabin_config_base_price: number;

    ac_type_name: string;

    tableImgUrl: string;

    public static fromJSON(json: IAcMacModel): AcMacModel {
        const vs = Object.create(AcMacModel.prototype);
        return Object.assign(vs, json, {
            tableImgUrl: AC_IMAGE_URL + 'aircraft_' + json.mac_id.toString() + '_tableimg.png',

        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? AcMacModel.fromJSON(value) : value;
    }

    public toJSON(): IAcMacModel {
        return Object.assign({}, this);
    }
}

export interface IAcMacModel {

    mac_id: number;
    mfr_id: number;
    model: string;
    ac_type_id: number;
    max_range_km: number;
    fuel_tank_l: number;
    pilots: number;
    crew_seats: number;
    max_seating: number;
    useful_load_kg: number;
    price: number;
    avg_fuel_consumption_lp100km: number;
    max_cruise_altitude_m: number;
    cruise_speed_kmph: number;
    take_off_m: number;
    landing_m: number;
    operation_time: string;
    cost_per_fh: number;
    asmi_rate: number;
    wiki_link: string;
    powerplant: string;
    production_start: string;
    production_rate_h: number;
    last_produced_on: string;
    notes: string;
    popularity: number;
    number_build: number;
    mtwo_kg: number;
    mtow_empty: number;
    seats_per_row: number;
    cabin_config_base_price: number;

    ac_type_name: string;

}
export class AcMacListModel {
    mac_list: Array<IAcMacModel>;
}

export interface RespAcMacList extends ResponseModel {
    data: AcMacListModel;
}
//#endregion
