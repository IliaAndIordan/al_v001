import { ResponseModel } from '../responce.model';

export class AirportRunwayModel {

    runway_id: number;

    airport_id: number;
    elevation_m: number;
    isFixed: number;
    mtow_kg: number;
    noise_restriction_level: number;
    radius: number;
    rdirection: string;
    rlenght_m: number;
    rsurface: string;
    rtype: string;
    rw_line: string;
    rw_nr: number;
    rw_rotation_degree: number;
    rwidth_m: number;
    svg_path_circle: string;
    hasTimeban: boolean;
    timeban_duractiont_h: number;
    timeban_start_h: number;
    usage_pct: number;

    pct_of_max_req_lenght: number;
    rcolor: string;


    public static fromJSON(json: IAirportRunwayModel): AirportRunwayModel {
        const vs = Object.create(AirportRunwayModel.prototype);
        return Object.assign(vs, json, {
            hasTimeban: json.timeban >= 1 ? true : false,
            isFixed: json.fixed >= 1 ? true : false,
            rdirection: json.rdirection ? json.rdirection.trim() : undefined,
            pct_of_max_req_lenght: json.rlenght_m ? (((json.rlenght_m / 3050) * 100) > 100 ? 100 : ((json.rlenght_m / 3050) * 100)) : 0,
            rcolor: ((json.rlenght_m / 3050) * 100) < 40 ? 'accent' : (((json.rlenght_m / 3050) * 100) > 80 ? 'primary' : 'warn'),
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? AirportRunwayModel.fromJSON(value) : value;
    }

    public toJSON(): IAirportRunwayModel {
        return Object.assign({}, this, {
            timeban: this.hasTimeban ? 1 : 0,
            fixed: this.isFixed ? 1 : 0,
        });
    }
}

export interface IAirportRunwayModel {

    runway_id: number;

    airport_id: number;
    elevation_m: number;
    fixed: number;
    mtow_kg: number;
    noise_restriction_level: number;
    radius: number;
    rdirection: string;
    rlenght_m: number;
    rsurface: string;
    rtype: string;
    rw_line: string;
    rw_nr: number;
    rw_rotation_degree: number;
    rwidth_m: number;
    svg_path_circle: string;
    timeban: number;
    timeban_duractiont_h: number;
    timeban_start_h: number;
    usage_pct: number;


}

export class IAirportRunwayListModel {
    rw_list: Array<IAirportRunwayModel>;

    getCountryModelList(): Array<AirportRunwayModel> {
        const rv = new Array<AirportRunwayModel>();
        if (this.rw_list) {
            this.rw_list.forEach((element: IAirportRunwayModel) => {


                const tmp: AirportRunwayModel = AirportRunwayModel.fromJSON(element);
                rv.push(tmp);
            });
        }

        return rv;
    }
}

export interface ResponseAirportRunwayListModel extends ResponseModel {
    data: IAirportRunwayListModel;
}

