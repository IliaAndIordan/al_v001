import { Injectable } from '@angular/core';
import { ApiHelper } from '../api.helper';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/observable';
import { tap, catchError } from 'rxjs/operators';
// Services
import { CurrentUserService } from '../../auth/current-user.service';
import { TokenService } from '../../auth/token.service';
// Constants
import { ServiceNames } from '../api.constants';
import { AirportRunwayModel, ResponseAirportRunwayListModel, IAirportRunwayModel } from './dto';
import { AmsStorageKeysLocal } from '../../const/ams-storage-key.const';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';

@Injectable()
export class AirportRunwayService {

    protected observable: Observable<any>;


    constructor(private apiHelper: ApiHelper,
        private http: HttpClient,
        private toastr: ToastrService,
        private currUserService: CurrentUserService,
        private tokenService: TokenService) { }

    //#region Data

    private get runways(): Array<AirportRunwayModel> {
        const runways = new Array<AirportRunwayModel>();
        const data = localStorage.getItem(AmsStorageKeysLocal.RunwayList);
        if (data) {
            const parsedData: Array<IAirportRunwayModel> = JSON.parse(data);
            parsedData.forEach((element: IAirportRunwayModel) => {
                const tmp: AirportRunwayModel = AirportRunwayModel.fromJSON(element);
                runways.push(tmp);
            });
        }
        return runways;
    }

    public resetRunwaysCache(): void {
        localStorage.removeItem(AmsStorageKeysLocal.RunwayList);
    }

    private updateRwCashe(data: Array<AirportRunwayModel>): void {

        if (data) {

            // console.log('updateRwCashe-> data.length=', data.length);

            const rw = this.runways;
            // console.log('updateRwCashe-> data', data);

            data.forEach((element: AirportRunwayModel) => {

                // console.log('updateRwCashe-> element', element);

                const toUpdate = rw.find(x => x.runway_id === element.runway_id);
                // console.log('updateRwCashe-> toUpdate', toUpdate);
                if (toUpdate) {
                    const idx = rw.indexOf(toUpdate);
                    rw[idx] = element;
                } else {
                    rw.push(element);
                }
            });
            // console.log('updateRwCashe-> runways', rw);
            localStorage.setItem(AmsStorageKeysLocal.RunwayList, JSON.stringify(rw));

        }
    }

    private airportRunwaysFromCache(apId: number): Array<AirportRunwayModel> {
        let data = new Array<AirportRunwayModel>();
        if (apId) {
            const runways = this.runways;
            if (runways && runways.length > 0) {
                data = runways.filter(x => x.airport_id === apId);
            }
        }

        return data;
    }

    public airportRunways(apId: number): Observable<Array<AirportRunwayModel>> {

        let data = new Array<AirportRunwayModel>();
        if (apId) {
            data = this.airportRunwaysFromCache(apId);
            if (!data || data.length === 0) {

                this.observable = this.getRunwayListByairport(apId)
                    .map((responce: ResponseAirportRunwayListModel) => {
                        this.observable = undefined;
                        data = new Array<AirportRunwayModel>();
                        if (responce.data.rw_list) {
                            responce.data.rw_list.forEach((element: IAirportRunwayModel) => {
                                const tmp: AirportRunwayModel = AirportRunwayModel.fromJSON(element);
                                data.push(tmp);
                            });
                            this.updateRwCashe(data);
                        }

                        return data;
                    })
                    .share();

            } else {
                // console.log('data already available');
                return Observable.of(data);
            }
        }

        return this.observable;
    }

    //#endregion

    //#region Runway Http Calls

    public getRunwayListByairport(airportId): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/ap_rw_list',
            { airport_id: airportId }, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`getRunwayListByairport`); }),
                catchError(this.handleError('getRunwayListByairport', []))
            );
    }

    //#endregion

    //#region URL Methods

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.airport);
    }

    //#endregion

    //#region Base

    /**
  * Handle Http operation that failed.
  * Let the app continue.
  * @param operation - name of the operation that failed
  * @param result - optional value to return as the observable result
  */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            // this.toastr.error(`${operation} failed: ${error.message}`, 'WAD Service');

            // Let the app keep running by returning an empty result.
            return Observable.of(result as T);
        };
    }

    /** Log a Service message  */
    private log(message: string) {
        // console.log('Airport Runway Service', message);
        // this.toastr.info(message, 'WAD Service');
    }

    //#endregion
}
