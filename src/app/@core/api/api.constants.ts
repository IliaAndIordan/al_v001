export const ServiceUrl = [
    {
        name: 'autenticate',
        url: 'http://ws.vlb.iordanov.info/login'
    },
    {
        name: 'refreshToken',
        url: 'http://ws.vlb.iordanov.info/jwtrefresh'
    },
    {
        name: 'airports_edit',
        url: 'http://wad.ws.ams.iordanov.info/airports_edit'
    },
    {
        name: 'airport',
        url: 'https://ams.ws.ams.iordanov.info/airport'
    },
    {
        name: 'airline',
        url: 'https://ams.ws.ams.iordanov.info/airline'
    },
    {
        name: 'aircraft',
        url: 'https://ams.ws.ams.iordanov.info/aircraft'
    },
];

export const ServiceNames = {
    autenticate: 'autenticate',
    refreshToken: 'refreshToken',
    airportsEdit: 'airports_edit',
    airport: 'airport',
    airline: 'airline',
    aircraft: 'aircraft',
};
