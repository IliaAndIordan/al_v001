import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class BaseService {

    constructor() { }



    //#region Base

    /**
  * Handle Http operation that failed.
  * Let the app continue.
  * @param operation - name of the operation that failed
  * @param result - optional value to return as the observable result
  */
    protected handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            // this.toastr.error(`${operation} failed: ${error.message}`, 'WAD Service');

            // Let the app keep running by returning an empty result.
            return Observable.of(result as T);
        };
    }

    /** Log a Service message  */
    protected log(message: string) {
        // console.log('Airport Runway Service', message);
        // this.toastr.info(message, 'WAD Service');
    }

    //#endregion
}
