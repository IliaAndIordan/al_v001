import { AppRoutes } from './app-routes.const';
import { UserRole } from '../auth/api/dto';


export const AmsNavSiteLeft = [
    {
        title: 'Home',
        url: '/' + AppRoutes.Public,
        icon: 'home',
        authorised: false,
        userRole: UserRole.User
    },
    {
        title: 'Dashboard',
        url: '/' + AppRoutes.Public,
        icon: 'dashboard',
        authorised: true,
        userRole: UserRole.User
    },
    {
        title: 'Country',
        url: '/' + AppRoutes.Country,
        icon: 'local_airport',
        authorised: true,
        userRole: UserRole.User
    },
    {
        title: 'AMS Reporter',
        url: '/' + AppRoutes.Newspaper,
        icon: 'wallpaper',
        authorised: false,
        userRole: UserRole.User
    },
];
