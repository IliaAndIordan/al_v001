export const AmsApplications = {
    WAD: {
        title: 'World Airport Database',
        abbreviation: 'WAD',
        logoUrl: 'https://common.iordanov.info/images/ams/common/logo.jpg',
        logoBigUrl: 'https://common.iordanov.info/images/ams/common/logo.jpg',
        // tslint:disable-next-line:max-line-length
        description: 'AMS World Airport Database is a web information system that contains data for almost every airport in the world, including airport codes, abbreviations, runway lengths and other airport details.',
        // tslint:disable-next-line:max-line-length
        note: 'AMS World Airport Database is a completely fictitious world, and any resemblance to real world airports, airport names, city names, country names, or otherwise, is completely coincidental.',
    },
    AMS: {
        title: 'Airline Management Simulator',
        abbreviation: 'AMS',
        logoUrl: 'https://common.iordanov.info/images/ams/common/logo.jpg',
        logoBigUrl: 'https://common.iordanov.info/images/ams/logo.jpg',
        description: 'AMS is a web information ' +
            'system that contains data for almost every airport in the world, ' +
            'including airport codes, abbreviations, runway lengths and other airport details.',

        note: 'AMS is a completely fictitious world, ' +
            'and any resemblance to real world airports, airport names, city names, ' +
            'country names, or otherwise, is completely coincidental.',
    }



};



