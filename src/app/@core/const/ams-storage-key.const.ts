export const AmsStorageKeysLocal = {
    CurrentUser: 'ams_local_user_key',
    UserLocation: 'ams_local_user_location',
    CurrentAirline: 'ams_local_airline_key',
    LocalUserSettings: 'ams_local_user_settings',
    Country: 'country_selected',
    CountryState: 'country_state_selected',
    /** airports chashe used in Airport Info Service */
    AirportInfoChahe: 'airports_cache',
    /* Airport list to show in table */
    AirportList: 'airport_list',
    RunwayList: 'airport_runwau_list',
    HqAirport: 'hq_aurport',
    CurrHubAirport: 'curr_hub_aurport',
    CurrDestAirport: 'curr_dest_aurport',
    AcMfrList: 'ac_mfr_list',
    CurrMfr: 'curr_mfr',
    AcMacList: 'ac_mac_list',
};
export const BASE_IMAGE_URL = 'https://common.iordanov.info/images/';
export const AMS_IMAGE_URL = BASE_IMAGE_URL + 'ams/';
export const COMMON_IMAGE_URL = AMS_IMAGE_URL + 'common/';
export const FLAGS_IMAGE_URL = BASE_IMAGE_URL + 'flags/';
export const AIRLINE_IMAGE_URL = BASE_IMAGE_URL + 'airline/';
export const AC_IMAGE_URL = BASE_IMAGE_URL + 'aircraft/';
export const AVATAR_IMAGE_URL = COMMON_IMAGE_URL + 'iziordanov_sd_128_bw.png';
export const AL_LOGO_IMAGE_URL = AIRLINE_IMAGE_URL + 'logo_1.png';
export const GMAP_API_KEY = 'AIzaSyDFFY0r9_fLApc2awmt-O1Q2URpkSFbQVc';

export const IMG_IATA_URL = COMMON_IMAGE_URL + 'iata.png';
export const IMG_ICAO_URL = COMMON_IMAGE_URL + 'icao.png';
export const IMG_WIKI_URL = COMMON_IMAGE_URL + 'wikipedia.ico';
export const IMG_OUAP_URL = COMMON_IMAGE_URL + 'ourairports.png';


