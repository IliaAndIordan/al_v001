export const AppRoutes = {
    Root: '',
    Newspaper: 'newspaper',
    Public: 'public',
    Dashboard: 'public',
    Country: 'country',
    Airline: 'airline',
    Aircrafts: 'aircraft',
    Manufacturers: 'manufacturers',
    Manufacturer: 'mfr',
    Mac: 'mac',
    AirlineCreate: 'airline',
};
