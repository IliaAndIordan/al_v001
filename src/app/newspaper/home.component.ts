import { Component, OnInit, ViewContainerRef } from '@angular/core';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private vcr: ViewContainerRef) {

  }

  ngOnInit() {
  }

}
