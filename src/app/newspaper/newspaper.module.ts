import { NgModule } from '@angular/core';
// Local Modules
import { SharedModule } from '../@share/shared.module';
import { NewspaperRoutingModule, RoutedComponents } from './routing.module';

@NgModule({
    imports: [
        NewspaperRoutingModule,
        SharedModule
    ],
    declarations: [
        RoutedComponents,
    ],
    exports: [
    ],
    entryComponents: []
})
export class NewspaperModule { }

